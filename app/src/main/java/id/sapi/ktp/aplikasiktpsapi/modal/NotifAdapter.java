package id.sapi.ktp.aplikasiktpsapi.modal;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.Calendar;
import java.util.List;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.activities.DetailLaporan;
import id.sapi.ktp.aplikasiktpsapi.database.LaporanDB;
import id.sapi.ktp.aplikasiktpsapi.edit.EditLaporan;
import id.sapi.ktp.aplikasiktpsapi.fragment.Laporan;

public class NotifAdapter extends RecyclerView.Adapter<NotifAdapter.NoteVH> {
    Context context;
    List<LaporanDB> laporanDBList;
    public int colorBefore;
    String date = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime()) ;

    LaporanAdapter.OnItemClickListener clickListener;

    public NotifAdapter(Context context, List<LaporanDB> laporanDBS) {
        this.context = context;
        this.laporanDBList = laporanDBS;

    }

    @Override
    public NotifAdapter.NoteVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notif_row, parent, false);
        NotifAdapter.NoteVH viewHolder = new NotifAdapter.NoteVH(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NotifAdapter.NoteVH holder, int position) {
        holder.id_pakan.setText(laporanDBList.get(position).getId_user());
        holder.nmpakan.setText(laporanDBList.get(position).getJudul_laporan());
        //holder.txstat.setText(laporanDBList.get(position).getTanggal());
        holder.txtgl.setText(laporanDBList.get(position).getUpdate_date());
        holder.txjml.setText(laporanDBList.get(position).getIsi_laporan());
        String letter = "A";

        if(laporanDBList.get(position).getId_user() != null && !laporanDBList.get(position).getId_user().isEmpty()) {
            letter = laporanDBList.get(position).getJudul_laporan().substring(0,1);
        }

        int color = holder.mColorGenerator.getRandomColor();

        // Create a circular icon consisting of  a random background colour and first letter of title
        holder.mDrawableBuilder = TextDrawable.builder()
                .buildRound(letter, color);
        holder.thumb.setImageDrawable(holder.mDrawableBuilder);
    }

    @Override
    public int getItemCount() {
        return laporanDBList.size();
    }

    class NoteVH extends RecyclerView.ViewHolder {
        TextView id_pakan, nmpakan, txjml, txstat, txtgl;
        ImageView edit, hapus, thumb;
        private TextDrawable mDrawableBuilder;
        private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;

        public NoteVH(final View itemView) {
            super(itemView);
            id_pakan = (TextView) itemView.findViewById(R.id.idPakan);
            nmpakan = (TextView) itemView.findViewById(R.id.pakan);
            txjml = (TextView) itemView.findViewById(R.id.jml);
            txstat = (TextView) itemView.findViewById(R.id.stat);
            txtgl = (TextView)itemView.findViewById(R.id.tgl);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            hapus = (ImageView) itemView.findViewById(R.id.hapus);
            thumb = (ImageView) itemView.findViewById(R.id.thumbnail_image);
            txjml.setVisibility(View.VISIBLE);
            txstat.setVisibility(View.GONE);
            txtgl.setVisibility(View.VISIBLE);
            edit.setVisibility(View.GONE);
            hapus.setVisibility(View.GONE);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // get position
                    int pos = getAdapterPosition();
                    // check if item still exists
                    if (pos != RecyclerView.NO_POSITION) {
                        LaporanDB clickedDataItem = laporanDBList.get(pos);
//                        Intent intent = new Intent(context, DetailLaporan.class);
//                        intent.putExtra("judul", laporanDBList.get(pos).getJudul_laporan());
//                        intent.putExtra("isi", laporanDBList.get(pos).getIsi_laporan());
//                        intent.putExtra("tanggal", laporanDBList.get(pos).getTanggal());
//                        context.startActivity(intent);
                        Toast.makeText(v.getContext(), "You clicked " + clickedDataItem.getUpdate_date(), Toast.LENGTH_SHORT).show();
                    }
                }

            });
        }
    }

}
