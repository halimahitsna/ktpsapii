package id.sapi.ktp.aplikasiktpsapi.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.eazegraph.lib.charts.ValueLineChart;
import org.eazegraph.lib.models.ValueLinePoint;
import org.eazegraph.lib.models.ValueLineSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.api.ApiService;
import id.sapi.ktp.aplikasiktpsapi.api.JSONResponse;
import id.sapi.ktp.aplikasiktpsapi.api.UtilsApi;
import id.sapi.ktp.aplikasiktpsapi.edit.EditData;
import id.sapi.ktp.aplikasiktpsapi.edit.EditProfil;
import id.sapi.ktp.aplikasiktpsapi.modal.Data;
import id.sapi.ktp.aplikasiktpsapi.modal.Jenis;
import id.sapi.ktp.aplikasiktpsapi.modal.JenisSpinner;
import id.sapi.ktp.aplikasiktpsapi.modal.Kandang;
import id.sapi.ktp.aplikasiktpsapi.modal.KandangSpinner;
import id.sapi.ktp.aplikasiktpsapi.modal.SapiSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GrafikLaporan extends AppCompatActivity {
    ActionBar actionBar;
    Toolbar toolbar;
    TextView tjudul, tgas, tkelembapan, tsuhu;
    ArrayList<Kandang> kandangs;
    ValueLineChart chartSuhu,chartKel, chartGas;
    private ArrayList<Kandang> data1;
    private ArrayList<Data> data2;
    Spinner kdg, kategori;
    String skdg, skat;
    LinearLayout line_grf;
    ProgressBar progressBar;
    List<String> kategoris;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafik_laporan);

        //Drawerbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        // actionBar.setDisplayShowHomeEnabled(true);
        koneksi();
        TextView judul = (TextView)findViewById(R.id.toolbar_title);
        kdg =(Spinner)findViewById(R.id.kandang);
        kategori = (Spinner)findViewById(R.id.kat);
        progressBar = (ProgressBar)findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);
        judul.setText("Grafik Laporan");
        chartSuhu= (ValueLineChart)findViewById(R.id.cubiclinechart);
        chartKel= (ValueLineChart)findViewById(R.id.cubiclinechart2);
        chartGas = (ValueLineChart)findViewById(R.id.cubiclinechart3);
        tsuhu = (TextView)findViewById(R.id.judul1);
        tkelembapan = (TextView)findViewById(R.id.judul2);
        tgas = (TextView)findViewById(R.id.judul3);

        line_grf =(LinearLayout)findViewById(R.id.grafik);
//        chartKel.setScaleY(100);
//        chartSuhu.setScaleY(100);
//        chartGas.setScaleY(300);
        line_grf.setVisibility(View.GONE);

        kategoris = new ArrayList<String>();
        kategoris.add("Pilih Kategori");
        kategoris.add("Sapi");
        kategoris.add("Kandang");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, kategoris);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        kategori.setAdapter(dataAdapter);
        kategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = kategoris.get(position).toString();
                skat = item;
                if(skat == "Sapi"){
                    initSapi();
                }else if(skat == "Kandang"){
                    initSpinner();
                }else if(skat == "Pilih Kategori"){
                    line_grf.setVisibility(View.GONE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                skat = "belum diatur";
            }
        });
        kdg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(data1.size()!=0) {
                    skdg = data1.get(position).getId_kandang();
                    loadJSONSuhu(skdg);
                }else if(data2.size() !=0){
                    skdg = data2.get(position).getId_sapi();
                    loadJSONSuhu("1");
                }else if(data1.size()==0){
                    skdg = "0";
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
                skdg = "belum diatur";
            }
        });


    }

    private void loadJSONSuhu(String idkandang)
    {
        line_grf.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService request = retrofit.create(ApiService.class);
        Call<JSONResponse> call = request.getGSuhu(idkandang);
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                JSONResponse jsonResponse = response.body();
                if (jsonResponse != null) {
                    progressBar.setVisibility(View.GONE);
                    kandangs = response.body().getSuhus();
                    if (kandangs.size() != 0) {
                        ValueLineSeries series = new ValueLineSeries();
                        series.setColor(0xFF56B7F1);
                        for (int i = 0; i < kandangs.size(); i++) {
                            Float nilai = Float.valueOf(kandangs.get(i).getSuhu());
                            series.addPoint(new ValueLinePoint("S " + i + "", nilai));
                            //series.
                        }
                        chartSuhu.addSeries(series);
                        chartSuhu.startAnimation();
                    }
                }else {
                    Toast.makeText(GrafikLaporan.this, "Tidak ada data suhu", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.d("Error", t.getMessage());
            }
        });

        //kelembapan
        Call<JSONResponse> call2 = request.getGKel(idkandang);
        call2.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call2, Response<JSONResponse> response) {
                JSONResponse jsonResponse = response.body();
                if (jsonResponse != null) {
                    kandangs  = response.body().getKelembapans();
                    if(kandangs.size() !=0) {
                        ValueLineSeries series = new ValueLineSeries();
                        series.setColor(0xFF56B7F1);
                        for (int i = 0; i < kandangs.size(); i++) {
                            Float nilai = Float.valueOf(kandangs.get(i).getKelembapan());
                            series.addPoint(new ValueLinePoint("Kel " + i + "", nilai));
                        }
                        chartKel.addSeries(series);
                        chartKel.startAnimation();
                    }else {
                        Toast.makeText(GrafikLaporan.this, "Tidak ada data kelembapan", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call2, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });

        //ammonia
        Call<JSONResponse> call3 = request.getGAmonia(idkandang);
        call3.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call3, Response<JSONResponse> response) {
                JSONResponse jsonResponse = response.body();
                if (jsonResponse != null) {
                    kandangs  = response.body().getAmonias();
                    if(kandangs.size() !=0) {
                        ValueLineSeries series = new ValueLineSeries();
                        series.setColor(0xFF56B7F1);
                        for (int i = 0; i < kandangs.size(); i++) {
                            Float nilai = Float.valueOf(kandangs.get(i).getGas_amonia());
                            series.addPoint(new ValueLinePoint("GA " + i + "", nilai));
                        }
                        chartGas.addSeries(series);
                        chartGas.startAnimation();
                    }else{
                        Toast.makeText(GrafikLaporan.this, "Tidak ada data gas amonia", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call3, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void initSpinner() {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService request = retrofit.create(ApiService.class);
        // Integer id = Integer.valueOf(sharedPrefManager.getSPId());
        Call<JSONResponse> call = request.getJSONKandang(getIntent().getStringExtra("id_user").trim());
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                if (response.isSuccessful()) {

                    JSONResponse jsonResponse = response.body();
                    data1 = new ArrayList<>(Arrays.asList(jsonResponse.getKandang()));
                    if(jsonResponse !=null) {
                        if (data1.size() != 0) {
                            ArrayList<Kandang> objects = new ArrayList<Kandang>();
                            //objects.add(0, je);
                            for (int i = 0; i < data1.size(); i++) {
                                Kandang obj = new Kandang();
                                obj.setAll(data1.get(i).getId_kandang(), data1.get(i).getKandang());
                                objects.add(obj);
                            }
                            // sjenis = data1.get(0).getId_jenis();
                            kdg.setPrompt("Pilih Kandang Sapi");
                            kdg.setAdapter(new KandangSpinner(GrafikLaporan.this, objects));
                        } else {
                            kdg.setAdapter(new ArrayAdapter<String>(GrafikLaporan.this, android.R.layout.simple_spinner_item, kategoris));

                        }
                    }

                } else {
                    Toast.makeText(GrafikLaporan.this, "Gagal mengambil data jenis", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void initSapi() {
        progressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService request = retrofit.create(ApiService.class);
        // Integer id = Integer.valueOf(sharedPrefManager.getSPId());
        Call<JSONResponse> call = request.getData(getIntent().getStringExtra("id_user").trim());
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                if (response.isSuccessful()) {

                    JSONResponse jsonResponse = response.body();
                    data2 = new ArrayList<>(Arrays.asList(jsonResponse.getData()));
                    if(data2.size() !=0) {
                        ArrayList<Data> objects = new ArrayList<Data>();
                        //objects.add(0, je);
                        for (int i = 0; i < data2.size(); i++) {
                            Data obj = new Data();
                            obj.setAll(data2.get(i).getId_sapi(), data2.get(i).getKodeRFID());
                            objects.add(obj);
                        }
                        // sjenis = data1.get(0).getId_jenis();
                        kdg.setPrompt("Pilih Sapi");
                        kdg.setAdapter(new SapiSpinner(GrafikLaporan.this, objects));
                    }else{
                        kdg.setAdapter(new ArrayAdapter<String>(GrafikLaporan.this, android.R.layout.simple_spinner_item, kategoris));
                    }
                } else {
                    Toast.makeText(GrafikLaporan.this, "Gagal mengambil data jenis", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    private boolean adaInternet(){
        ConnectivityManager koneks = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneks.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
//            Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(GrafikLaporan.this, "Tidak ada koneksi internet", Toast.LENGTH_LONG).show();

        }
    }
}
