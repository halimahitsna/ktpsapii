package id.sapi.ktp.aplikasiktpsapi.tambah;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.api.ApiService;
import id.sapi.ktp.aplikasiktpsapi.api.JSONResponse;
import id.sapi.ktp.aplikasiktpsapi.api.UtilsApi;
import id.sapi.ktp.aplikasiktpsapi.edit.EditData;
import id.sapi.ktp.aplikasiktpsapi.modal.Data;
import id.sapi.ktp.aplikasiktpsapi.modal.Jenis;
import id.sapi.ktp.aplikasiktpsapi.modal.Kandang;
import id.sapi.ktp.aplikasiktpsapi.modal.KandangSpinner;
import id.sapi.ktp.aplikasiktpsapi.modal.Pakan;
import id.sapi.ktp.aplikasiktpsapi.modal.PakanSpinner;
import id.sapi.ktp.aplikasiktpsapi.util.AlarmReceiver;
import id.sapi.ktp.aplikasiktpsapi.util.Reminder;
import id.sapi.ktp.aplikasiktpsapi.util.ReminderDatabase;
import id.sapi.ktp.aplikasiktpsapi.util.SharedPrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class TambahJadwal extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener{
        private Toolbar mToolbar;
        ActionBar actionBar;
        private EditText mTitleText;
        private TextView mtitle, mDateText, mTimeText, mRepeatText, mRepeatNoText, mRepeatTypeText, kandangtext, jmltext, pakantext, cttntext, cttnset,idusertext;
        private FloatingActionButton mFAB1;
        private FloatingActionButton mFAB2;
        SharedPrefManager sharedPrefManager;
        Button simpan;
        Spinner spakan;
        private Calendar mCalendar;
        private int mYear, mMonth, mHour, mMinute, mDay;
        private long mRepeatTime;
        private String mTitle,iduser;
        private String sKandang;
        private String sPakan,pkn;
        private String sJumlah;
        private String mNote;
        private String mTime;
        private String mDate;
        private String mRepeat;
        private String mRepeatNo;
        private String mRepeatType;
        private String mActive;
        private ArrayList<Pakan> data1;
        private ArrayList<Kandang> data2;

        // Values for orientation change
        private static final String KEY_TITLE = "title_key";
        private static final String KEY_TIME = "time_key";
        private static final String KEY_DATE = "date_key";
        private static final String KEY_IDUSER = "iduser_key";
        private static final String KEY_KANDANG = "kandang_key";
        private static final String KEY_PAKAN = "pakan_key";
        private static final String KEY_JUMLAH = "jumlah_key";
        private static final String KEY_CATATAN = "catatan_key";
        private static final String KEY_REPEAT = "repeat_key";
        private static final String KEY_REPEAT_NO = "repeat_no_key";
        private static final String KEY_REPEAT_TYPE = "repeat_type_key";
        private static final String KEY_ACTIVE = "active_key";

        // Constant values in milliseconds
        private static final long milMinute = 60000L;
        private static final long milHour = 3600000L;
        private static final long milDay = 86400000L;
        private static final long milWeek = 604800000L;
        private static final long milMonth = 2592000000L;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_tambah_jadwal);

            sharedPrefManager = new SharedPrefManager(TambahJadwal.this);
            iduser = getIntent().getStringExtra("id_user");
            new StyleableToast
                    .Builder(getApplicationContext())
                    .text(iduser)
                    .iconStart(R.drawable.ic_check_black_24dp)
                    .textColor(Color.WHITE)
                    .backgroundColor(Color.BLUE)
                    .show();

            // Initialize Views
            mtitle = (TextView)findViewById(R.id.toolbar_title);
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            mTitleText = (EditText) findViewById(R.id.reminder_title);
            mDateText = (TextView) findViewById(R.id.set_date);
            mTimeText = (TextView) findViewById(R.id.set_time);
            mRepeatText = (TextView) findViewById(R.id.set_repeat);
            mRepeatNoText = (TextView) findViewById(R.id.set_repeat_no);
            mRepeatTypeText = (TextView) findViewById(R.id.set_repeat_type);
            kandangtext = (TextView)findViewById(R.id.set_kandang) ;
            pakantext = (TextView)findViewById(R.id.set_pakan) ;
            cttntext = (TextView)findViewById(R.id.cttn_text);
            cttnset = (TextView)findViewById(R.id.set_cttn);
            jmltext = (TextView)findViewById(R.id.set_jml);
            idusertext = (TextView)findViewById(R.id.iduser);
            spakan = (Spinner)findViewById(R.id.pakan_text);
            mFAB1 = (FloatingActionButton) findViewById(R.id.starred1);
            mFAB2 = (FloatingActionButton) findViewById(R.id.starred2);
            simpan = (Button)findViewById(R.id.btnSimpan);

            // Setup Toolbar
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            actionBar = getSupportActionBar();
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            mtitle.setText("Tambah Jadwal");

            // Initialize default values
            kandangtext.setText("Kandang");
            pakantext.setText("Pakan");
            jmltext.setText("0");
            mActive = "true";
            mRepeat = "true";
            mRepeatNo = Integer.toString(1);
            mRepeatType = "Jam";

            mCalendar = Calendar.getInstance();
            mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
            mMinute = mCalendar.get(Calendar.MINUTE);
            mYear = mCalendar.get(Calendar.YEAR);
            mMonth = mCalendar.get(Calendar.MONTH) + 1;
            mDay = mCalendar.get(Calendar.DATE);

            mDate = mDay + "/" + mMonth + "/" + mYear;
            mTime = mHour + ":" + mMinute;
            sPakan = "Pakan Sapi";

            // Setup Reminder Title EditText
            mTitleText.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mTitle = s.toString().trim();
                    mTitleText.setError(null);
                }

                @Override
                public void afterTextChanged(Editable s) {}
            });

            // Setup TextViews using reminder values
            mDateText.setText(mDate);
            mTimeText.setText(mTime);
            mRepeatNoText.setText(mRepeatNo);
            mRepeatTypeText.setText(mRepeatType);
            mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType + " sekali");

            // To save state on device rotation
            if (savedInstanceState != null) {
                String savedTitle = savedInstanceState.getString(KEY_TITLE);
                mTitleText.setText(savedTitle);
                mTitle = savedTitle;

                String savedTime = savedInstanceState.getString(KEY_TIME);
                mTimeText.setText(savedTime);
                mTime = savedTime;

                String savedPakan= savedInstanceState.getString(KEY_PAKAN);
                pakantext.setText(savedPakan);
                sPakan = savedPakan;

                String savedDate = savedInstanceState.getString(KEY_DATE);
                mDateText.setText(savedDate);
                mDate = savedDate;

                String saveRepeat = savedInstanceState.getString(KEY_REPEAT);
                mRepeatText.setText(saveRepeat);
                mRepeat = saveRepeat;

                String savedRepeatNo = savedInstanceState.getString(KEY_REPEAT_NO);
                mRepeatNoText.setText(savedRepeatNo);
                mRepeatNo = savedRepeatNo;

                String savedRepeatType = savedInstanceState.getString(KEY_REPEAT_TYPE);
                mRepeatTypeText.setText(savedRepeatType);
                mRepeatType = savedRepeatType;

                String savedKandang = savedInstanceState.getString(KEY_KANDANG);
               kandangtext.setText(savedKandang);
                sKandang = savedKandang;

                String savedIdUser = savedInstanceState.getString(KEY_IDUSER);
                iduser = savedIdUser;

                String savedJml = savedInstanceState.getString(KEY_JUMLAH);
                jmltext.setText(savedJml);
                sJumlah = savedJml;

                String savedNote = savedInstanceState.getString(KEY_CATATAN);
                cttntext.setText(savedNote);
                mNote = savedNote;

                mActive = savedInstanceState.getString(KEY_ACTIVE);
            }

            // Setup up active buttons
            if (mActive.equals("false")) {
                mFAB1.setVisibility(View.VISIBLE);
                mFAB2.setVisibility(View.GONE);

            } else if (mActive.equals("true")) {
                mFAB1.setVisibility(View.GONE);
                mFAB2.setVisibility(View.VISIBLE);
            }

            simpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mTitleText.setText(mTitle);

                    if (mTitleText.getText().toString().length() == 0)
                        mTitleText.setError("Judul tidak boleh kosong");

                    else {
                        saveReminder();
                    }
                }
            });
        }

        // To save state on device rotation
        @Override
        protected void onSaveInstanceState (Bundle outState) {
            super.onSaveInstanceState(outState);

            outState.putCharSequence(KEY_TITLE, mTitleText.getText());
            outState.putCharSequence(KEY_TIME, mTimeText.getText());
            outState.putCharSequence(KEY_DATE, mDateText.getText());
            outState.putCharSequence(KEY_PAKAN, pakantext.getText());
            outState.putCharSequence(KEY_IDUSER,iduser);
            outState.putCharSequence(KEY_KANDANG, kandangtext.getText());
            outState.putCharSequence(KEY_JUMLAH, jmltext.getText());
            outState.putCharSequence(KEY_CATATAN, cttntext.getText());
            outState.putCharSequence(KEY_REPEAT, mRepeatText.getText());
            outState.putCharSequence(KEY_REPEAT_NO, mRepeatNoText.getText());
            outState.putCharSequence(KEY_REPEAT_TYPE, mRepeatTypeText.getText());
            outState.putCharSequence(KEY_ACTIVE, mActive);
        }

        // On clicking Time picker
        public void setTime(View v){
            Calendar now = Calendar.getInstance();
            TimePickerDialog tpd = TimePickerDialog.newInstance(
                    this,
                    now.get(Calendar.HOUR_OF_DAY),
                    now.get(Calendar.MINUTE),
                    false
            );
            tpd.setThemeDark(false);
            tpd.show(getFragmentManager(), "Timepickerdialog");
        }

        // On clicking Date picker
        public void setDate(View v){
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.show(getFragmentManager(), "Datepickerdialog");
        }

        // Obtain time from time picker
        @Override
        public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
            mHour = hourOfDay;
            mMinute = minute;
            if (minute < 10) {
                mTime = hourOfDay + ":" + "0" + minute;
            } else {
                mTime = hourOfDay + ":" + minute;
            }
            mTimeText.setText(mTime);
        }

        // Obtain date from date picker
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            monthOfYear ++;
            mDay = dayOfMonth;
            mMonth = monthOfYear;
            mYear = year;
            mDate = dayOfMonth + "/" + monthOfYear + "/" + year;
            mDateText.setText(mDate);
        }

        // On clicking the active button
        public void selectFab1(View v) {
            mFAB1 = (FloatingActionButton) findViewById(R.id.starred1);
            mFAB1.setVisibility(View.GONE);
            mFAB2 = (FloatingActionButton) findViewById(R.id.starred2);
            mFAB2.setVisibility(View.VISIBLE);
            mActive = "true";
        }

        // On clicking the inactive button
        public void selectFab2(View v) {
            mFAB2 = (FloatingActionButton) findViewById(R.id.starred2);
            mFAB2.setVisibility(View.GONE);
            mFAB1 = (FloatingActionButton) findViewById(R.id.starred1);
            mFAB1.setVisibility(View.VISIBLE);
            mActive = "false";
        }

        // On clicking the repeat switch
        public void onSwitchRepeat(View view) {
            boolean on = ((Switch) view).isChecked();
            if (on) {
                mRepeat = "true";
                mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType + " sekali");
            } else {
                mRepeat = "false";
                mRepeatText.setText("Tidak ada pengulangan");
            }
        }

        // On clicking repeat type button
        public void selectRepeatType(View v){
            final String[] items = new String[5];

            items[0] = "Menit";
            items[1] = "Jam";
            items[2] = "Hari";
            items[3] = "Minggu";
            items[4] = "Bulan";

            // Create List Dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Pilih tipe");
            builder.setItems(items, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int item) {

                    mRepeatType = items[item];
                    mRepeatTypeText.setText(mRepeatType);
                    mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType + " sekali");
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        // On clicking repeat interval button
        public void setRepeatNo(View v){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Ketikkan angka");

            // Create EditText box to input repeat number
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            alert.setView(input);
            alert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            if (input.getText().toString().length() == 0) {
                                mRepeatNo = Integer.toString(1);
                                mRepeatNoText.setText(mRepeatNo);
                                mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType + " sekali");
                            }
                            else {
                                mRepeatNo = input.getText().toString().trim();
                                mRepeatNoText.setText(mRepeatNo);
                                mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType + " sekali");
                            }
                        }
                    });
            alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // do nothing
                }
            });
            alert.show();
        }
        public void setJumlah(View v){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Ketikkan jumlah");

            // Create EditText box to input repeat number
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            alert.setView(input);
            alert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            if (input.getText().toString().length() == 0) {
                                sJumlah = Integer.toString(1);
                                jmltext.setText(sJumlah +" kg");
                               // mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType + " sekali");
                            }
                            else {
                                sJumlah = input.getText().toString().trim();
                                jmltext.setText(sJumlah +" kg");
                               // mRepeatText.setText("Setiap " + mRepeatNo + " " + mRepeatType + " sekali");
                            }
                        }
                    });
            alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // do nothing
                }
            });
            alert.show();
        }

        public void setPakan(View view) {
           initSpinnerPakan();
        }


        // On clicking the save button
        public void saveReminder(){
            ReminderDatabase rb = new ReminderDatabase(this);

            // Creating Reminder
            int ID = rb.addReminder(new Reminder( mTitle, mDate, mTime, mRepeat, mRepeatNo, mRepeatType, mActive,mNote,iduser,sPakan,sJumlah,sKandang));

            // Set up calender for creating the notification
            mCalendar.set(Calendar.MONTH, --mMonth);
            mCalendar.set(Calendar.YEAR, mYear);
            mCalendar.set(Calendar.DAY_OF_MONTH, mDay);
            mCalendar.set(Calendar.HOUR_OF_DAY, mHour);
            mCalendar.set(Calendar.MINUTE, mMinute);
            mCalendar.set(Calendar.SECOND, 0);

            // Check repeat type
            if (mRepeatType.equals("Menit")) {
                mRepeatTime = Integer.parseInt(mRepeatNo) * milMinute;
            } else if (mRepeatType.equals("Jam")) {
                mRepeatTime = Integer.parseInt(mRepeatNo) * milHour;
            } else if (mRepeatType.equals("Hari")) {
                mRepeatTime = Integer.parseInt(mRepeatNo) * milDay;
            } else if (mRepeatType.equals("Minggu")) {
                mRepeatTime = Integer.parseInt(mRepeatNo) * milWeek;
            } else if (mRepeatType.equals("Bulan")) {
                mRepeatTime = Integer.parseInt(mRepeatNo) * milMonth;
            }

            // Create a new notification
            if (mActive.equals("true")) {
                if (mRepeat.equals("true")) {
                    new AlarmReceiver().setRepeatAlarm(getApplicationContext(), mCalendar, ID, mRepeatTime);
                } else if (mRepeat.equals("false")) {
                    new AlarmReceiver().setAlarm(getApplicationContext(), mCalendar, ID);
                }
            }

            // Create toast to confirm new reminder
            new StyleableToast
                    .Builder(getApplicationContext())
                    .text("Data Jadwal Tersimpan")
                    .iconStart(R.drawable.ic_check_black_24dp)
                    .textColor(Color.WHITE)
                    .backgroundColor(Color.BLUE)
                    .show();
            onBackPressed();
        }

                // On pressing the back button
        @Override
        public void onBackPressed() {
            super.onBackPressed();
        }

        // Creating the menu
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_back, menu);
            return true;
        }

        // On clicking menu buttons
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            if (item.getItemId() == android.R.id.home) {
                onBackPressed();
                return true;
            }
            return false;
        }

    private void initSpinnerPakan() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService request = retrofit.create(ApiService.class);
        Call<JSONResponse> call = request.getPakan(iduser);
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                if (response.isSuccessful()) {
                    JSONResponse jsonResponse = response.body();
                    data1 = new ArrayList<>(Arrays.asList(jsonResponse.getPakan()));
                    ArrayList<Pakan> objects = new ArrayList<Pakan>();
                    for (int i = 0; i < data1.size(); i++) {
                        Pakan obj = new Pakan();
                        obj.setAll(data1.get(i).getId_pakan(), data1.get(i).getPakan());
                        objects.add(obj);
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(TambahJadwal.this);
                    builder.setTitle("Pilih Pakan Sapi");
                    builder.setAdapter(new PakanSpinner(TambahJadwal.this, objects), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            pakantext.setText(data1.get(i).getPakan());
                            sPakan = data1.get(i).getPakan();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    new StyleableToast
                            .Builder(getApplicationContext())
                            .text("Gagal mengambil data")
                            .iconStart(R.drawable.ic_cloud_off_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.RED)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }
    private void initKandang() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService request = retrofit.create(ApiService.class);
        // Integer id = Integer.valueOf(sharedPrefManager.getSPId());
        Call<JSONResponse> call = request.getJSONKandang(iduser);
        call.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {
                if (response.isSuccessful()) {
                    JSONResponse jsonResponse = response.body();
                    data2 = new ArrayList<>(Arrays.asList(jsonResponse.getKandang()));
                    ArrayList<Kandang> objects = new ArrayList<Kandang>();
                    for (int i = 0; i < data2.size(); i++) {
                        Kandang obj = new Kandang();
                        obj.setAll(data2.get(i).getId_kandang(), data2.get(i).getKandang());
                        objects.add(obj);
                    }
                    final AlertDialog.Builder builder = new AlertDialog.Builder(TambahJadwal.this);
                    builder.setTitle("Pilih Kandang Sapi");
                    builder.setAdapter(new KandangSpinner(TambahJadwal.this, objects), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            kandangtext.setText(data2.get(i).getKandang());
                            sKandang = data2.get(i).getKandang();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();

                } else {
                    new StyleableToast
                            .Builder(getApplicationContext())
                            .text("Gagal mengambil data")
                            .iconStart(R.drawable.ic_close_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.RED)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    public void setKandang(View view) {
            initKandang();
    }

    public void klikNote(View view) {
        // Creating alert Dialog with one Button
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(TambahJadwal.this);

        //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Catatan");

        // Setting Dialog Message
        alertDialog.setMessage("Ketikkan catatan disini");
        final EditText input = new EditText(TambahJadwal.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);

        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.ic_collections_bookmark_black_24dp);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        // Write your code here to execute after dialog
                        mNote = input.getText().toString();
                        cttnset.setText(mNote);

                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show();
    }
}