package id.sapi.ktp.aplikasiktpsapi.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.squareup.picasso.Picasso;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.api.ApiService;
import id.sapi.ktp.aplikasiktpsapi.api.UtilsApi;
import id.sapi.ktp.aplikasiktpsapi.modal.Result;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailMonitoringKandang extends AppCompatActivity {
    private static final String TAG = DetailMonitoringKandang.class.getSimpleName();
    Toolbar toolbar;
    ActionBar actionBar;
    TextView txtid, txtkandang, txtsuhu, txtkelembapan, txtgas, txtbsuhu;
    Switch swotomatis, swkipas, swlampu;
    ImageView foto;
    String idkan, stlampu, stkipas;
    double suhu, kelembapan, gas;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    int redColorValue = Color.RED;
    int blackColorValue = Color.BLACK;
    String lamp ;
    String fan;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_monitoring_kandang);
        Intent i = getIntent();
        idkan = i.getStringExtra("id_kandang");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        txtkandang = (TextView) findViewById(R.id.kandang);
        txtsuhu = (TextView) findViewById(R.id.suhu);
        txtkelembapan = (TextView) findViewById(R.id.kelembapan);
        txtgas = (TextView) findViewById(R.id.gas);
        swotomatis = (Switch) findViewById(R.id.oto);
        swkipas = (Switch) findViewById(R.id.kipas);
        swlampu = (Switch) findViewById(R.id.lampu);
        foto = (ImageView) findViewById(R.id.foto);

        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        swotomatis.setChecked(sharedpreferences.getBoolean("swOtomatis", false));
        swlampu.setChecked(sharedpreferences.getBoolean("swLampu", false));
        swkipas.setChecked(sharedpreferences.getBoolean("swKipas", false));

        //updateotomatis
        if(sharedpreferences.getBoolean("swOtomatis", false) == true ){
            updateOtomatis();
        }

        txtkandang.setText(getIntent().getStringExtra("kandang"));
        txtsuhu.setText(getIntent().getStringExtra("suhu") + " Celcius");
        txtkelembapan.setText(getIntent().getStringExtra("kelembapan") + " %");
        txtgas.setText(getIntent().getStringExtra("gas") + " PPm");
        Picasso.with(this).load(getIntent().getStringExtra("foto")).into(foto);
        if (getIntent().getStringExtra("suhu") != "0" || getIntent().getStringExtra("kelembapan") != "0" || getIntent().getStringExtra("suhu") != "0") {
            //suhu = Double.parseDouble(i.getStringExtra("suhu"));
            suhu = Float.valueOf(getIntent().getStringExtra("suhu"));
            kelembapan = Float.valueOf(getIntent().getStringExtra("kelembapan"));
            gas = Float.valueOf(getIntent().getStringExtra("gas"));
        } else {
            suhu = 0;
            kelembapan = 0;
            gas = 0;
        }
        if(suhu >27 || suhu < 17 || kelembapan > 70 || kelembapan < 60 || gas < 0 || gas > 25){
            txtsuhu.setTextColor(redColorValue);
            txtkelembapan.setTextColor(redColorValue);
            txtgas.setTextColor(redColorValue);
        }else {
            txtsuhu.setTextColor(blackColorValue);
            txtkelembapan.setTextColor(blackColorValue);
            txtgas.setTextColor(blackColorValue);
        }

        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        swotomatis.setChecked(sharedpreferences.getBoolean("swOtomatis", false));

        swotomatis.setTextOn("ON");
        swotomatis.setTextOff("OFF");
        swkipas.setTextOn("ON");
        swkipas.setTextOff("OFF");
        swlampu.setTextOn("ON");
        swlampu.setTextOff("OFF");
        swotomatis.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("swOtomatis", true);
                    editor.commit();
                    new StyleableToast
                            .Builder(DetailMonitoringKandang.this)
                            .text("Pengendali Otomatis Hidup")
                            .iconStart(R.drawable.ic_check_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.BLUE)
                            .show();
                    if (suhu < 17) {
                        stlampu = "1";
                        stkipas = "0";
                        updateOtomatis();
                    } else if (suhu > 27) {
                        stkipas = "1";
                        stlampu = "0";
                        updateOtomatis();
                    } else {
                        stlampu = "0";
                        stkipas = "0";
                        updateOtomatis();
                    }
                } else {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("swOtomatis", false);
                    editor.commit();
                    stlampu = "0";
                    stkipas = "0";
                    updateOtomatis();
                    // Show the switch button checked status as toast message
                    new StyleableToast
                            .Builder(DetailMonitoringKandang.this)
                            .text("Pengendali Otomatis Mati")
                            .iconStart(R.drawable.ic_close_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.RED)
                            .show();
                }
            }
        });

        swkipas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("swKipas", true);
                    editor.commit();
                    // Show the switch button checked status as toast messag
                    stkipas = "1";
                    updateKipas();

                } else {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("swKipas", false);
                    editor.commit();
                    stkipas = "0";
                    updateKipas();
                }
            }
        });

        swlampu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // Show the switch button checked status as toast message
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("swLampu", true);
                    editor.commit();
                    stlampu = "1";
                    updateLampu();
                } else {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean("swLampu", false);
                    editor.commit();
                    stlampu = "0";
                    updateLampu();
                }
            }
        });


    }

    private void updateOtomatis() {
        koneksi();
        String id = idkan.toString().trim();
        if(stkipas == null || stlampu == null){
            lamp = "0";
            fan = "0";
        }else {
            lamp = stlampu.trim();
            fan = stkipas.trim();
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService api = retrofit.create(ApiService.class);
        Call<Result> call = api.updateOtomatis(getIntent().getStringExtra("id_kandang").trim(), lamp, fan);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
                if (value.equals("1")) {
                    new StyleableToast
                            .Builder(DetailMonitoringKandang.this)
                            .text("lampu : " +lamp + " kipas : "+ fan)
                            .iconStart(R.drawable.ic_check_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.BLUE)
                            .show();
                          } else {
                    new StyleableToast
                            .Builder(DetailMonitoringKandang.this)
                            .text(message)
                            .iconStart(R.drawable.ic_close_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.RED)
                            .show();
                }
              //  updateLampu();
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                // progress.dismiss();
                new StyleableToast
                        .Builder(DetailMonitoringKandang.this)
                        .text("Jaringan Error")
                        .iconStart(R.drawable.ic_cloud_off_black_24dp)
                        .textColor(Color.WHITE)
                        .backgroundColor(Color.RED)
                        .show();
            }
        });
    }

    private void updateLampu() {
        koneksi();
        String id = idkan.toString().trim();
        final String lamp = stlampu.trim();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService api = retrofit.create(ApiService.class);
        Call<Result> call = api.updateLampu(id, lamp);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
                if (value.equals("1")) {
                    new StyleableToast
                            .Builder(DetailMonitoringKandang.this)
                            .text("Lampu Menyala")
                            .iconStart(R.drawable.ic_check_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.BLUE)
                            .show();
                } else {
                    new StyleableToast
                            .Builder(DetailMonitoringKandang.this)
                            .text("Lampu Mati")
                            .iconStart(R.drawable.ic_close_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.RED)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                new StyleableToast
                        .Builder(DetailMonitoringKandang.this)
                        .text("Jaringan Error")
                        .iconStart(R.drawable.ic_close_black_24dp)
                        .textColor(Color.WHITE)
                        .backgroundColor(Color.RED)
                        .show();
            }
        });
        // onBackPressed();
    }

    private void updateKipas() {
        koneksi();
        String id = idkan.toString().trim();
        final String kip = stkipas.trim();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService api = retrofit.create(ApiService.class);
        Call<Result> call = api.updateKipas(id, kip);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
                if (value.equals("1")) {
                    new StyleableToast
                            .Builder(DetailMonitoringKandang.this)
                            .text("Kipas Menyala")
                            .iconStart(R.drawable.ic_check_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.BLUE)
                            .show();
                } else {
                    new StyleableToast
                            .Builder(DetailMonitoringKandang.this)
                            .text("Kipas Mati")
                            .iconStart(R.drawable.ic_close_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.RED)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                new StyleableToast
                        .Builder(DetailMonitoringKandang.this)
                        .text("Jaringan Error")
                        .iconStart(R.drawable.ic_check_black_24dp)
                        .textColor(Color.WHITE)
                        .backgroundColor(Color.BLUE)
                        .show();
            }
        });
        // onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean adaInternet(){
        ConnectivityManager koneksi = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
//            Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            new StyleableToast
                    .Builder(DetailMonitoringKandang.this)
                    .text("Tidak ada koneksi internet")
                    .iconStart(R.drawable.ic_check_black_24dp)
                    .textColor(Color.WHITE)
                    .backgroundColor(Color.BLUE)
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

}
