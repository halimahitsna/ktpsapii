package id.sapi.ktp.aplikasiktpsapi.util;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.format.DateUtils;
import android.widget.RemoteViews;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.activities.NotifDismiss;
import id.sapi.ktp.aplikasiktpsapi.activities.NotificationIntentService;
import id.sapi.ktp.aplikasiktpsapi.database.LaporanDB;
import id.sapi.ktp.aplikasiktpsapi.edit.EditJadwal;
import id.sapi.ktp.aplikasiktpsapi.fragment.JadwalMakan;

import static android.support.v4.content.res.ResourcesCompat.getColor;
import static android.support.v4.content.res.ResourcesCompat.getDrawable;
import static android.support.v4.content.res.TypedArrayUtils.getString;


public class AlarmReceiver extends WakefulBroadcastReceiver {
    AlarmManager mAlarmManager;
    PendingIntent mPendingIntent;
    private int numMessages = 0;
    final int NOTIFICATION_ID = 1;
    SharedPrefManager sharedPrefManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        int mReceivedID = Integer.parseInt(intent.getStringExtra(EditJadwal.EXTRA_REMINDER_ID));
        sharedPrefManager = new SharedPrefManager(context);
        // Get notification title from Reminder Database
        ReminderDatabase rb = new ReminderDatabase(context);
        Reminder reminder = rb.getReminder(mReceivedID);
        String mTitle = reminder.getTitle();
        String mTgl = reminder.getDate();
        String mWkt = reminder.getTime();
        String mPakan = reminder.getmPakan();
        String mJumlah = reminder.getmJumlah();
        String mKandang = reminder.getmKandang();

        // Create intent to open ReminderEditActivity on notification click
        Intent buttonIntent = new Intent(context, EditJadwal.class);
        buttonIntent.putExtra("notificationId",NOTIFICATION_ID);

//Create the PendingIntent
        PendingIntent btPendingIntent = PendingIntent.getBroadcast(context, 0, buttonIntent,0);

        Intent editIntent = new Intent(context, EditJadwal.class);
        editIntent.putExtra(EditJadwal.EXTRA_REMINDER_ID, Integer.toString(mReceivedID));
        PendingIntent mClick = PendingIntent.getActivity(context, mReceivedID, editIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Create Notification
        /*NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_date_range_black_24dp))
                .setSmallIcon(R.drawable.sapi2)
                .setContentTitle(context.getResources().getString(R.string.app_name))
                .setTicker(mTitle)
                .setContentText(mTitle)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(mClick)
                .setAutoCancel(true)
                .setOnlyAlertOnce(true);

        NotificationManager nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(mReceivedID, mBuilder.build());*/
        /*end notif*/

        /*custom notif*/
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(mTitle)
                .setContentText(mTitle)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
                .setContentIntent(mClick)
                .setContentInfo("Hello")
                .setLights(Color.BLUE, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setNumber(++numMessages)
                .setSmallIcon(R.drawable.ic_notifications_black_24dp);

        RemoteViews expandedView = new RemoteViews(context.getPackageName(), R.layout.notification_jadwal);
        expandedView.setTextViewText(R.id.timestamp, mWkt);
        // expandedView.setTextViewText(R.id.notification_message, data.get("title"));
        expandedView.setTextViewText(R.id.content_title, "Jadwal Pakan");
        expandedView.setTextViewText(R.id.content_text, mTitle);
        expandedView.setTextViewText(R.id.notification_message, "Waktu          "+mWkt+"\nTanggal       "+mTgl+"\nPakan          "+mPakan+ "\nJumlah        "+mJumlah+" kg\nKandang       "+mKandang);
        // adding action to left button
        //Intent leftIntent = new Intent(context, NotificationIntentService.class);
        Intent leftIntent = new Intent(context, EditJadwal.class);
        leftIntent.setAction("left");
        //expandedView.setOnClickPendingIntent(R.id.left_button, PendingIntent.getService(getApplicationContext(), 0, leftIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        expandedView.setOnClickPendingIntent(R.id.left_button, btPendingIntent);

        // adding action to right button
        Intent rightIntent = new Intent(context, EditJadwal.class);
        rightIntent.setAction("right");

        //expandedView.setOnClickPendingIntent(R.id.right_button, PendingIntent.getService(getApplicationContext(), 1, rightIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        expandedView.setOnClickPendingIntent(R.id.right_button, btPendingIntent);
        RemoteViews collapsedView = new RemoteViews(context.getPackageName(), R.layout.view_collapsed_jadwal);
        collapsedView.setTextViewText(R.id.content_title, mTitle);
        collapsedView.setTextViewText(R.id.content_text, "Expand for details notifications");
        collapsedView.setTextViewText(R.id.timestamp, mWkt);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                // these are the three things a NotificationCompat.Builder object requires at a minimum
                .setSmallIcon(R.drawable.sapi2)
                .setContentTitle(mTitle)
                .setContentText(mKandang)
                // notification will be dismissed when tapped
                .setAutoCancel(true)
                // tapping notification will open MainActivity
                //.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), MainActivity.class), 0))
                .setContentIntent(mPendingIntent)
                // setting the custom collapsed and expanded views
                .setCustomContentView(collapsedView)
                .setCustomBigContentView(expandedView)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                // setting style to DecoratedCustomViewStyle() is necessary for custom views to display
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle());


        NotificationManager nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(mReceivedID, builder.build());
        /*end custom*/
        String date = DateUtils.formatDateTime(context, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME).toString().trim();
        LaporanDB notif = new LaporanDB(mTitle.toString().trim(), mPakan.toString().trim()+", "+mJumlah.toString().trim()+"kg, " +mKandang.toString().trim(),date.trim(),"notif", sharedPrefManager.getSPId().toString().trim());
        notif.save();

    }

    public void setAlarm(Context context, Calendar calendar, int ID) {
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // Put Reminder ID in Intent Extra
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EditJadwal.EXTRA_REMINDER_ID, Integer.toString(ID));
        mPendingIntent = PendingIntent.getBroadcast(context, ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Calculate notification time
        Calendar c = Calendar.getInstance();
        long currentTime = c.getTimeInMillis();
        long diffTime = calendar.getTimeInMillis() - currentTime;

        // Start alarm using notification time
        mAlarmManager.set(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + diffTime,
                mPendingIntent);

        // Restart alarm if device is rebooted
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public void setRepeatAlarm(Context context, Calendar calendar, int ID, long RepeatTime) {
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // Put Reminder ID in Intent Extra
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(EditJadwal.EXTRA_REMINDER_ID, Integer.toString(ID));
        mPendingIntent = PendingIntent.getBroadcast(context, ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Calculate notification timein
        Calendar c = Calendar.getInstance();
        long currentTime = c.getTimeInMillis();
        long diffTime = calendar.getTimeInMillis() - currentTime;

        // Start alarm using initial notification time and repeat interval time
        mAlarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + diffTime,
                RepeatTime , mPendingIntent);

        // Restart alarm if device is rebooted
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public void cancelAlarm(Context context, int ID) {
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // Cancel Alarm using Reminder ID
        mPendingIntent = PendingIntent.getBroadcast(context, ID, new Intent(context, AlarmReceiver.class), 0);
        mAlarmManager.cancel(mPendingIntent);

        // Disable alarm
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}