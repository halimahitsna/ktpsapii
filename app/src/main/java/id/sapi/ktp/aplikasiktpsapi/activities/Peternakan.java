package id.sapi.ktp.aplikasiktpsapi.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.api.ApiService;
import id.sapi.ktp.aplikasiktpsapi.api.JSONResponse;
import id.sapi.ktp.aplikasiktpsapi.api.UtilsApi;
import id.sapi.ktp.aplikasiktpsapi.edit.EditProfil;
import id.sapi.ktp.aplikasiktpsapi.modal.Result;
import id.sapi.ktp.aplikasiktpsapi.util.SharedPrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Peternakan extends AppCompatActivity {
    ActionBar actionBar;
    Toolbar toolbar;
    Button btnsimpan;
    EditText eid, enama, elamat;
    Context mcontext;
    Retrofit apiService;
    String iduser, idpeternakan;
    SharedPrefManager sharedPrefManager;
    public ArrayList<id.sapi.ktp.aplikasiktpsapi.modal.Peternakan> data1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peternakan);

        sharedPrefManager = new SharedPrefManager(this);
        Intent i = getIntent();
        iduser = i.getStringExtra("id_user");

        mcontext = this;
        apiService = UtilsApi.getClient();
        //Drawerbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView judul = (TextView)findViewById(R.id.toolbar_title);
        judul.setText("Edit Peternakan");
        eid = (EditText)findViewById(R.id.idu);
        elamat = (EditText)findViewById(R.id.Ealamat) ;
        enama = (EditText)findViewById(R.id.Enamapeternakan);
        btnsimpan = (Button)findViewById(R.id.btnSimpan);
        loadPeternakan();
        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update();
            }
        });
    }

    private void loadPeternakan() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService request1 = retrofit.create(ApiService.class);
        //String id = sharedPrefManager.getSPId());
        Call<JSONResponse> call1 = request1.getPeternakan(iduser);
        call1.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call1, Response<JSONResponse> response) {
                JSONResponse jsonResponse1 = response.body();
                data1 = new ArrayList<>(Arrays.asList(jsonResponse1.getPeternakan()));
                if(jsonResponse1.getPeternakan().length != 0) {
                    enama.setText(data1.get(0).getPeternakan());
                    elamat.setText(data1.get(0).getAlamat());
                    eid.setText(data1.get(0).getId_peternakan());
                }else {
                    enama.setText("Nama Peternakan");
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }
    private void update() {
        String id = eid.getText().toString().trim();
        String alm = elamat.getText().toString().trim();
        String nm = enama.getText().toString().trim();

        //validate();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService api = retrofit.create(ApiService.class);
        Call<Result> call = api.updatePeternakan(id, nm, alm);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
//                loading.dismiss();
                if (value.equals("1")) {
                    Toast.makeText(Peternakan.this, message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Peternakan.this, message, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                // progress.dismiss();
                Toast.makeText(Peternakan.this, "Jaringan Error!", Toast.LENGTH_SHORT).show();
            }
        });
        onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}
