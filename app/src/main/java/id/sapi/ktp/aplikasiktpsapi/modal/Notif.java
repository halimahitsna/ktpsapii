package id.sapi.ktp.aplikasiktpsapi.modal;

import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class Notif extends SugarRecord {
    @Unique
    private Long id_notif;
    private String title;
    private String message;
    private String idkandang;
    private String kandang;
    private String foto;
    private String suhu;
    private String kelembapan;

    public Notif(String title, String message) {
        this.title = title;
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdkandang() {
        return idkandang;
    }

    public void setIdkandang(String idkandang) {
        this.idkandang = idkandang;
    }

    public String getKandang() {
        return kandang;
    }

    public void setKandang(String kandang) {
        this.kandang = kandang;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getSuhu() {
        return suhu;
    }

    public void setSuhu(String suhu) {
        this.suhu = suhu;
    }

    public String getKelembapan() {
        return kelembapan;
    }

    public void setKelembapan(String kelembapan) {
        this.kelembapan = kelembapan;
    }

    public Long getId_notif() {
        return id_notif;
    }

    public void setId_notif(Long id_notif) {
        this.id_notif = id_notif;
    }
}
