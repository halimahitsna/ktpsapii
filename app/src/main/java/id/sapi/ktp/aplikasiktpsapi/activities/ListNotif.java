package id.sapi.ktp.aplikasiktpsapi.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.orm.SugarDb;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.database.LaporanDB;
import id.sapi.ktp.aplikasiktpsapi.fragment.Laporan;
import id.sapi.ktp.aplikasiktpsapi.modal.LaporanAdapter;
import id.sapi.ktp.aplikasiktpsapi.modal.Notif;
import id.sapi.ktp.aplikasiktpsapi.modal.NotifAdapter;

public class ListNotif extends AppCompatActivity {
    NotifAdapter adapter;
    RecyclerView recyclerView;
    Toolbar toolbar;
    ActionBar actionBar;
    String iduser;
    SwipeRefreshLayout swipeRefreshLayout;
    long initialCount;
    List<LaporanDB> laporans = new ArrayList<>();
    TextView tkoneksi;

    int modifyPos = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_jenis);
        SugarDb db = new SugarDb(getApplicationContext());
        db.onCreate(db.getDB());
        Intent i = getIntent();
        iduser = i.getStringExtra("id_user");

        //Drawerbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView tjudul = (TextView)findViewById(R.id.toolbar_title);
        tjudul.setText("Notifikasi");
        recyclerView = (RecyclerView)findViewById(R.id.card_recycle_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(layoutManager);

        tkoneksi = (TextView) findViewById(R.id.txtkoneksi);
        tkoneksi.setVisibility(View.INVISIBLE);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        if (savedInstanceState != null)
            modifyPos = savedInstanceState.getInt("notif");

        initialCount = LaporanDB.count(LaporanDB.class);
        if (initialCount == 0) {
            if (laporans.isEmpty()) {
                tkoneksi.setVisibility(View.VISIBLE);
                tkoneksi.setText("Tidak ada data notif");
                Snackbar.make(recyclerView, "Tidak ada data notifikasi.", Snackbar.LENGTH_LONG).show();
            } else {
                tkoneksi.setVisibility(View.GONE);
                loadJSON();
            }
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadJSON();
            }
        });
    }

    public void loadJSON() {
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.Card1, R.color.Card2, R.color.Card3, R.color.Card4);
        List<LaporanDB> findWithCustomQuery = Select.from(LaporanDB.class).where(Condition.prop("jenis").eq("notif")).and(Condition.prop("iduser").eq(iduser)).orderBy("idlaporan DESC").list();
        //List<LaporanDB> findWithCustomQuery =LaporanDB.findWithQuery(LaporanDB.class, "SELECT * FROM LaporanDB WHERE jenis=? ORDER BY id_laporan DESC", "notif");
        adapter = new NotifAdapter(getApplication(), findWithCustomQuery);
        //Toast.makeText(getApplication(), String.valueOf(LaporanDB.count(LaporanDB.class)), Toast.LENGTH_SHORT).show();
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        tkoneksi.setVisibility(View.GONE);
        loadJSON();

    }

    @Override
    public void onResume() {
        super.onResume();
        loadJSON();
        tkoneksi.setVisibility(View.GONE);
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notif, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }else if(item.getItemId() == R.id.action_clear){
            Toast.makeText(getApplication(), "Data Notifikasi berhasil terhapus", Toast.LENGTH_SHORT).show();
            LaporanDB.deleteAll(LaporanDB.class,"jenis = ?", "notif");
            Snackbar.make(recyclerView, "Tidak ada data notifikasi.", Snackbar.LENGTH_LONG).show();
            loadJSON();
        }

        return super.onOptionsItemSelected(item);
    }

}