package id.sapi.ktp.aplikasiktpsapi.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import id.sapi.ktp.aplikasiktpsapi.modal.DRfid;
import id.sapi.ktp.aplikasiktpsapi.modal.Data;
import id.sapi.ktp.aplikasiktpsapi.modal.DataRfid;
import id.sapi.ktp.aplikasiktpsapi.modal.Indukan;
import id.sapi.ktp.aplikasiktpsapi.modal.Jadwal;
import id.sapi.ktp.aplikasiktpsapi.modal.Jenis;
import id.sapi.ktp.aplikasiktpsapi.modal.Kandang;
import id.sapi.ktp.aplikasiktpsapi.modal.Pakan;
import id.sapi.ktp.aplikasiktpsapi.modal.Penyakit;
import id.sapi.ktp.aplikasiktpsapi.modal.Peternakan;
import id.sapi.ktp.aplikasiktpsapi.modal.User;

/**
 * Created by hali on 25/08/2017.
 */

public class JSONResponse {
    private Kandang[]kandang;
    private Jenis[]jenis;
    private Peternakan[]peternakan;
    private User[]users;
    private Pakan[]pakan;
    private Penyakit[]penyakit;
    private Indukan[]indukan;
    private Jadwal[]jadwal;
    private Data[]data;
    private DataRfid[]rfid;
    private DRfid[]rfids;

    public Kandang[]getKandang(){return kandang;}
    public Jenis[]getJenis(){return jenis;}
    public Peternakan[]getPeternakan(){return peternakan;}
    public User[]getUsers(){return users;}
    public Pakan[]getPakan(){return pakan;}
    public Penyakit[]getPenyakit(){return penyakit;}
    public Indukan[]getIndukan(){return indukan;}
    public Jadwal[]getJadwal(){return jadwal;}
    public Data[]getData(){return data;}
    public DataRfid[]getRfid(){return rfid;}
    public DRfid[]getRfids(){return rfids;}

    @SerializedName("suhu")
    @Expose
    private ArrayList<Kandang> suhus = new ArrayList<>();

    public ArrayList<Kandang> getSuhus() {
        return suhus;
    }

    @SerializedName("kelembapan")
    @Expose
    private ArrayList<Kandang> kelembapans = new ArrayList<>();

    public ArrayList<Kandang> getKelembapans() {
        return kelembapans;
    }

    @SerializedName("amonia")
    @Expose
    private ArrayList<Kandang> amonias = new ArrayList<>();

    public ArrayList<Kandang> getAmonias() {
        return amonias;
    }
}
