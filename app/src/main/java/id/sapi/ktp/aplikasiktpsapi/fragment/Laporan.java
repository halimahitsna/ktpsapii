package id.sapi.ktp.aplikasiktpsapi.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.orm.SugarDb;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

import az.plainpie.PieView;
import az.plainpie.animation.PieAngleAnimation;
import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.activities.GrafikLaporan;
import id.sapi.ktp.aplikasiktpsapi.database.LaporanDB;
import id.sapi.ktp.aplikasiktpsapi.edit.EditLaporan;
import id.sapi.ktp.aplikasiktpsapi.modal.LaporanAdapter;
import id.sapi.ktp.aplikasiktpsapi.tambah.TambahKandang;
import id.sapi.ktp.aplikasiktpsapi.tambah.TambahLaporan;
import id.sapi.ktp.aplikasiktpsapi.util.SharedPrefManager;

public class Laporan extends Fragment {
    LaporanAdapter adapter;
    RecyclerView recyclerView;
    AddFloatingActionButton add;
    SharedPrefManager sharedPrefManager;
    RelativeLayout Rgraphic;
    String iduser;
    SwipeRefreshLayout swipeRefreshLayout;
    long initialCount;
    List<LaporanDB> laporans = new ArrayList<>();
    TextView tkoneksi;

    int modifyPos = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_laporan, container, false);
        SugarDb db = new SugarDb(getContext());
        db.onCreate(db.getDB());
        return rootView;
    }


    @SuppressLint({"ResourceAsColor", "ResourceType"})
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Laporan");
        koneksi();
        sharedPrefManager = new SharedPrefManager(getActivity());
        iduser = sharedPrefManager.getSPId().toString();

        Rgraphic = (RelativeLayout)view.findViewById(R.id.graph);
        recyclerView = (RecyclerView)view.findViewById(R.id.card_recycle_view);
        StaggeredGridLayoutManager gridLayoutManager =
                new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        gridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS);
        recyclerView.setLayoutManager(gridLayoutManager);

        tkoneksi = (TextView)view.findViewById(R.id.txtkoneksi);
        tkoneksi.setVisibility(View.INVISIBLE);

        recyclerView.setLayoutManager(gridLayoutManager);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swiperefresh);
        Rgraphic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent b = new Intent(getActivity(), GrafikLaporan.class);
                b.putExtra("id_user", iduser);
                startActivity(b);
            }
        });
        add = (AddFloatingActionButton) view.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(getActivity(), TambahLaporan.class);
                a.putExtra("id_user", iduser);
                startActivity(a);
            }
        });

        if (savedInstanceState != null)
            modifyPos = savedInstanceState.getInt("modify");

        initialCount = LaporanDB.count(LaporanDB.class);
        if (initialCount == 0) {
            if (laporans.isEmpty()){
                tkoneksi.setVisibility(View.VISIBLE);
                tkoneksi.setText("Tidak ada data laporan");
                //Snackbar.make(recyclerView, "Tidak ada data laporan.", Snackbar.LENGTH_LONG).show();
            }else{
                tkoneksi.setVisibility(View.GONE);
                loadJSON();
            }
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadJSON();
            }
        });
    }

    public void loadJSON(){
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.Card1, R.color.Card2, R.color.Card3, R.color.Card4);
        List<LaporanDB> findWithCustomQuery =Select.from(LaporanDB.class).where(Condition.prop("iduser").eq(iduser)).and(Condition.prop("jenis").eq("laporan")).list();
        //laporans = LaporanDB.listAll(LaporanDB.class);
        //laporans = LaporanDB.findWithQuery(LaporanDB.class, "Select * from LaporanDB where id_user = ?", iduser);
        //laporans = Select.from(LaporanDB.class).where(Condition.prop("id_user").eq(iduser)).list();
        swipeRefreshLayout.setRefreshing(false);
        adapter = new LaporanAdapter(getActivity(), findWithCustomQuery);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        tkoneksi.setVisibility(View.GONE);
        loadJSON();

    }

    @Override
    public void onResume(){
        super.onResume();
        loadJSON();
        tkoneksi.setVisibility(View.GONE);
    }

    private boolean adaInternet(){
        ConnectivityManager koneksi = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }

    private void koneksi(){
        if(adaInternet()){
//            Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            new StyleableToast
                    .Builder(getActivity())
                    .text("Tidak Ada Koneksi Internet")
                    .iconStart(R.drawable.ic_cloud_off_black_24dp)
                    .textColor(Color.WHITE)
                    .backgroundColor(Color.RED)
                    .show();
            tkoneksi.setVisibility(View.VISIBLE);
            tkoneksi.setText("Tidak ada koneksi internet!");
        }
    }
}
