package id.sapi.ktp.aplikasiktpsapi.tambah;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.squareup.picasso.Picasso;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.api.ApiService;
import id.sapi.ktp.aplikasiktpsapi.api.BaseResponse;
import id.sapi.ktp.aplikasiktpsapi.api.UtilsApi;
import id.sapi.ktp.aplikasiktpsapi.modal.Result;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TambahKandang extends AppCompatActivity {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    EditText txtid, txtkandang;
    Button btnsimpan;
    Toolbar toolbars;
    ActionBar actionBar;
    String iduser, imagePath;
    TextView tjudul;
    CircleImageView foto;
    Context mcontext;
    FloatingActionButton add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_kandang);

        mcontext = this;
        Intent i = getIntent();
        iduser = i.getStringExtra("id_user");
        verifyStoragePermissions(this);
        txtid = (EditText) findViewById(R.id.idKandang);
        txtkandang = (EditText)findViewById(R.id.kandang);
        btnsimpan = (Button)findViewById(R.id.btnSimpan);
        foto = (CircleImageView) findViewById(R.id.foto);
        add = (FloatingActionButton)findViewById(R.id.btnfoto);
        tjudul = (TextView)findViewById(R.id.toolbar_title);
        tjudul.setText("Tambah Kandang");
        //Picasso.with(mcontext).load(R.drawable.ic_business_black_24dp).resize(150, 150).into(foto);

        //Drawerbar
        toolbars = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbars);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        txtid.setText(getIntent().getStringExtra("id_kandang"));
        txtkandang.setText(getIntent().getStringExtra("kandang"));

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_PICK);

                final Intent chooserIntent = Intent.createChooser(galleryIntent, "Choose image");
                startActivityForResult(chooserIntent, 100);
            }
        });

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    if(imagePath!=null) {
                        simpan();
                        uploadImage();
                    }else if(imagePath ==null) {
                        simpan();
                    }
                }
            }
        });
    }

    private boolean validate(){
        boolean valid = true;
        String kdg = txtkandang.getText().toString();

        if(kdg.isEmpty()) {
            txtkandang.setError("Nama kandang harus diisi!");
            valid = false;
        }
        return valid;
    }

    private void simpan() {
        koneksi();
        String id = iduser.toString().trim();
        String kd = txtkandang.getText().toString().trim();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService api = retrofit.create(ApiService.class);
        Call<Result> call = api.insertKandang(id, kd);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
                //loading.dismiss();
                if (value.equals("1")) {
                    new StyleableToast
                            .Builder(TambahKandang.this)
                            .text(message)
                            .iconStart(R.drawable.ic_check_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.BLUE)
                            .show();
                } else {
                    new StyleableToast
                            .Builder(TambahKandang.this)
                            .text(message)
                            .iconStart(R.drawable.ic_cloud_off_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.RED)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                // progress.dismiss();
                new StyleableToast
                        .Builder(TambahKandang.this)
                        .text("Jaringan Errorr")
                        .iconStart(R.drawable.ic_cloud_off_black_24dp)
                        .textColor(Color.WHITE)
                        .backgroundColor(Color.RED)
                        .show();
            }
        });
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void uploadImage() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ktpsapi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        File file = new File(imagePath);

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        MultipartBody.Part text = MultipartBody.Part.createFormData("id_kandang", txtid.getText().toString().trim());

        ApiService request = retrofit.create(ApiService.class);
        Call<BaseResponse> call = request.uploadFotoKandang(body,text);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()==true) {
                        new StyleableToast
                                .Builder(TambahKandang.this)
                                .text(response.body().getMessage())
                                .iconStart(R.drawable.ic_check_black_24dp)
                                .textColor(Color.WHITE)
                                .backgroundColor(Color.BLUE)
                                .show();
                    }else
                        new StyleableToast
                                .Builder(TambahKandang.this)
                                .text(response.body().getMessage())
                                .iconStart(R.drawable.ic_close_black_24dp)
                                .textColor(Color.WHITE)
                                .backgroundColor(Color.RED)
                                .show();

                } else {
                    new StyleableToast
                            .Builder(TambahKandang.this)
                            .text(response.body().getMessage())
                            .iconStart(R.drawable.ic_close_black_24dp)
                            .textColor(Color.WHITE)
                            .backgroundColor(Color.RED)
                            .show();
                }

                //ifoto.setImageDrawable(null);
                imagePath = null;
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
            }
        });
        onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 100) {
            if (data == null) {

                new StyleableToast
                        .Builder(TambahKandang.this)
                        .text("Unable to pick image")
                        .iconStart(R.drawable.ic_close_black_24dp)
                        .textColor(Color.WHITE)
                        .backgroundColor(Color.RED)
                        .show();
                return;
            }

            Uri imageUri = data.getData();
            foto.setImageURI(imageUri);
            imagePath =getRealPathFromURI(imageUri);

        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    private boolean adaInternet(){
        ConnectivityManager koneksi = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
//            Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            new StyleableToast
                    .Builder(TambahKandang.this)
                    .text("Tidak Ada Koneksi Internet")
                    .iconStart(R.drawable.ic_cloud_off_black_24dp)
                    .textColor(Color.WHITE)
                    .backgroundColor(Color.RED)
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}
