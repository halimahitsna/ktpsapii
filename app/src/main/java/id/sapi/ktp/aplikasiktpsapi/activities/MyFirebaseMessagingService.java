
package id.sapi.ktp.aplikasiktpsapi.activities;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.api.ApiService;
import id.sapi.ktp.aplikasiktpsapi.api.UtilsApi;
import id.sapi.ktp.aplikasiktpsapi.database.LaporanDB;
import id.sapi.ktp.aplikasiktpsapi.modal.Notif;
import id.sapi.ktp.aplikasiktpsapi.modal.Result;
import id.sapi.ktp.aplikasiktpsapi.util.SharedPrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public static final String FCM_PARAM = "picture";
    private static final String CHANNEL_NAME = "FCM";
    private static final String CHANNEL_DESC = "Firebase Cloud Messaging";
    private int numMessages = 0;
    private static final String TAG = "MyFirebaseMsgingService";
    SharedPreferences sharedPreferencesNotif;
    SharedPrefManager sharedPrefManager;
    String ttime;

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        sharedPreferencesNotif = getSharedPreferences(TAG,
                Context.MODE_PRIVATE);
        sharedPrefManager = new SharedPrefManager(this);
        RemoteMessage.Notification notification = remoteMessage.getNotification();
        Map<String, String> data = remoteMessage.getData();
        //tambahan
         String stkipas = null, stlampu=null;
        Log.d("FROM", remoteMessage.getFrom());
        //getFromSharedPref
        SharedPreferences updateOto = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        Boolean pref1=updateOto.getBoolean("swOtomatis", false);
        if(pref1 == true) {
            updateOtomatis(data, stkipas, stlampu);
        }
        SharedPreferences preferences = getSharedPreferences("notif", MODE_PRIVATE);
        if(preferences.getBoolean("NotifStatus", false) == true) {
            sendNotification(notification, data);
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void sendNotification(RemoteMessage.Notification notification, Map<String, String> data) {
        Bundle bundle = new Bundle();
        bundle.putString(FCM_PARAM, data.get(FCM_PARAM));
        SharedPreferences myPrefs = getSharedPreferences("mypref", MODE_PRIVATE);
        myPrefs.getBoolean("swOtomatis", false);
        Intent intent = new Intent(this, DetailMonitoringKandang.class);
        bundle.putString("id_kandang", data.get("idkandang"));
        bundle.putString("kandang", data.get("kandang"));
        bundle.putString("suhu",data.get("suhu") );
        bundle.putString("kelembapan", data.get("kelembapan"));
        bundle.putString("gas", data.get("gas"));
        bundle.putString("foto", data.get("foto"));
        bundle.putString("iduser", data.get("iduser"));
        intent.putExtras(bundle);

        String idkan = data.get("idkandang");

        int notificationId = new Random().nextInt(); // just use a counter in some util class...
        //PendingIntent dismissIntent = NotifDismiss.getDismissIntent(notificationId, getApplicationContext());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                .setContentTitle(data.get("title"))
                .setContentText(data.get("message"))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
                .setContentIntent(pendingIntent)
                .setContentInfo("Hello")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.sapi2))
//                .setColor(getColor(R.color.colorPrimary))
                .setLights(Color.BLUE, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setNumber(++numMessages)
                .setSmallIcon(R.drawable.ic_notifications_black_24dp);

        RemoteViews expandedView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.custom_notifications);
        expandedView.setTextViewText(R.id.timestamp, DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME));
       // expandedView.setTextViewText(R.id.notification_message, data.get("title"));
        expandedView.setTextViewText(R.id.content_title, data.get("title"));
        expandedView.setTextViewText(R.id.content_text, data.get("message"));
        expandedView.setTextViewText(R.id.notification_message, data.get("detail"));
        // adding action to left button
        Intent leftIntent = new Intent(getApplicationContext(), NotificationIntentService.class);
        leftIntent.setAction("left");
        //expandedView.setOnClickPendingIntent(R.id.left_button, PendingIntent.getService(getApplicationContext(), 0, leftIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        expandedView.setOnClickPendingIntent(R.id.left_button, pendingIntent);

        // adding action to right button
        Intent rightIntent = new Intent(getApplicationContext(), NotificationIntentService.class);
        rightIntent.setAction("right");

        //expandedView.setOnClickPendingIntent(R.id.right_button, PendingIntent.getService(getApplicationContext(), 1, rightIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        expandedView.setOnClickPendingIntent(R.id.right_button, PendingIntent.getService(getApplicationContext(), 1, rightIntent, PendingIntent.FLAG_CANCEL_CURRENT));
        RemoteViews collapsedView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.view_collapsed_notification);
        collapsedView.setTextViewText(R.id.content_title, data.get("title"));
        collapsedView.setTextViewText(R.id.content_text, "Expand for details notifications");
        collapsedView.setTextViewText(R.id.timestamp, DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME));
        //ttime = DateUtils.formatDateTime(getApplicationContext(), System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME).toString();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                // these are the three things a NotificationCompat.Builder object requires at a minimum
                .setSmallIcon(R.drawable.sapi2)
                .setContentTitle(data.get("title"))
                .setContentText(data.get("message"))
                // notification will be dismissed when tapped
                .setAutoCancel(true)
                // tapping notification will open MainActivity
                //.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, new Intent(getApplicationContext(), MainActivity.class), 0))
                .setContentIntent(pendingIntent)
                // setting the custom collapsed and expanded views
                .setCustomContentView(collapsedView)
                .setCustomBigContentView(expandedView)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                // setting style to DecoratedCustomViewStyle() is necessary for custom views to display
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle());

        try {
            String picture = data.get(FCM_PARAM);
            if (picture != null && !"".equals(picture)) {
                URL url = new URL(picture);
                Bitmap bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                builder.setStyle(
                        new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(notification.getBody())
                );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // retrieves android.app.NotificationManager
        NotificationManager notificationManager = (android.app.NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    getString(R.string.notification_channel_id), CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription(CHANNEL_DESC);
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});

            assert notificationManager != null;
            notificationManager.notify(0, builder.build());
        }

        assert notificationManager != null;
        notificationManager.notify(0, builder.build());

        String date = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime()).toString().trim() ;
        LaporanDB notif = new LaporanDB(data.get("title").toString().trim(), data.get("message").toString().trim(),date, "notif", sharedPrefManager.getSPId().toString().trim());
        notif.save();
    }

    private void updateOtomatis(Map<String, String> data, String stkipas, String stlampu) {
        //koneksi();
        String id;
        if(data.get("idkandang")!= null)
            id = data.get("idkandang").toString().trim();
        else
            id = "0";

        if(Float.valueOf(data.get("suhu")) >27 ){
            stlampu = "0";
            stkipas = "1";
        }else  if(Float.valueOf(data.get("suhu")) <17 ){
            stlampu = "1";
            stkipas = "0";
        }else{
            stlampu = "0";
            stkipas = "0";
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService api = retrofit.create(ApiService.class);
        Call<Result> call = api.updateOtomatis(id,stlampu.trim() , stkipas.trim());
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
                if (value.equals("1")) {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                   // Toast.makeText(getApplicationContext(), "lampu : " +stlampu + " kipas : "+ fan, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                }
                //  updateLampu();
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                // progress.dismiss();
                Toast.makeText(getApplicationContext(), "Jaringan Error!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
       /* NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, getString(R.string.notification_channel_id))
                .setContentTitle(data.get("title"))
                .setContentText(data.get("dudu"))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                //.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.win))
                .setContentIntent(pendingIntent)
                .setContentInfo("Hello")
                .setStyle(new NotificationCompat.BigTextStyle()
                .bigText(data.get("dudu")))
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_warning_black_24dp))
                .setColor(getColor(R.color.colorPrimary))
                .setLights(Color.BLUE, 1000, 300)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setNumber(++numMessages)
                .setSmallIcon(R.drawable.sapi2);

        try {
            String picture = data.get(FCM_PARAM);
            if (picture != null && !"".equals(picture)) {
                URL url = new URL(picture);
                Bitmap bigPicture = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                notificationBuilder.setStyle(
                        new NotificationCompat.BigPictureStyle().bigPicture(bigPicture).setSummaryText(notification.getBody())
                );
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    getString(R.string.notification_channel_id), CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription(CHANNEL_DESC);
            channel.setShowBadge(true);
            channel.canShowBadge();
            channel.enableLights(true);
            channel.setLightColor(Color.RED);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});

            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }

        assert notificationManager != null;
        notificationManager.notify(0, notificationBuilder.build());
    }
}
*/

