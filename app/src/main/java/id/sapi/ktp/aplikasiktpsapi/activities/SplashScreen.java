package id.sapi.ktp.aplikasiktpsapi.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.daimajia.androidanimations.library.Techniques;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import id.sapi.ktp.aplikasiktpsapi.R;

public class SplashScreen extends Activity{
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent intent = new Intent(SplashScreen.this,HalamanLogin.class);
                intent.putExtra("berhasil",1);
                overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
                startActivity(intent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);


    }

    /*@Override
    public void initSplash(ConfigSplash configSplash) {
        configSplash.setBackgroundColor(R.color.background);
        configSplash.setLogoSplash(R.drawable.sapi);
        configSplash.setAnimLogoSplashDuration(1500);
        configSplash.setAnimLogoSplashTechnique(Techniques.BounceIn);
        configSplash.setTitleTextSize(0);
    }

    @Override
    public void animationsFinished() {
        Intent intent = new Intent(getApplicationContext(),HalamanLogin.class);
        intent.putExtra("berhasil",1);
        startActivity(intent);
        finish();
    }*/

}
