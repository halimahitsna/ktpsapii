package id.sapi.ktp.aplikasiktpsapi.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Arrays;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.api.ApiService;
import id.sapi.ktp.aplikasiktpsapi.api.JSONResponse;
import id.sapi.ktp.aplikasiktpsapi.api.UtilsApi;
import id.sapi.ktp.aplikasiktpsapi.modal.DataRfid;
import id.sapi.ktp.aplikasiktpsapi.modal.KandangAdapter;
import id.sapi.ktp.aplikasiktpsapi.modal.Peternakan;
import id.sapi.ktp.aplikasiktpsapi.modal.Result;
import id.sapi.ktp.aplikasiktpsapi.tambah.TambahData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TambahRfid extends AppCompatActivity {
    //UPDATE `rfid` SET `id_sapi`='abc' WHERE id=(SELECT id FROM rfid ORDER BY input_date DESC LIMIT 1)
    Toolbar toolbars;
    ActionBar actionBar;
    String iduser;
    AVLoadingIndicatorView avLoadingIndicatorView;
    AddFloatingActionButton add;
    Button stop;
    TextView tinfo;
    public ArrayList<DataRfid> data1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_rfid);
        Intent i = getIntent();
        iduser = i.getStringExtra("id_user");

        //Drawerbar
        toolbars = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbars);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);
        avLoadingIndicatorView = (AVLoadingIndicatorView)findViewById(R.id.loading);
        add = (AddFloatingActionButton)findViewById(R.id.addRFID);
        stop = (Button)findViewById(R.id.btnstop);
        tinfo = (TextView)findViewById(R.id.infotext);
        //tinfo.setText("Tambah Kode dari RFID");
        stop.setVisibility(View.INVISIBLE);
        avLoadingIndicatorView.setVisibility(View.INVISIBLE);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                avLoadingIndicatorView.smoothToShow();
                tinfo.setText("Scan RFID Tag");
                tambahRFID();
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do something after 5s = 5000ms
                        getRFID();
                    }
                }, 8000);
                add.setVisibility(View.INVISIBLE);
                stop.setVisibility(View.VISIBLE);
            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tinfo.setText("Tambah Kode dari RFID");
                avLoadingIndicatorView.smoothToHide();
                add.setVisibility(View.VISIBLE);
                stop.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void tambahRFID() {
       // iduser = sharedPrefManager.getSPId();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService request = retrofit.create(ApiService.class);
        Call<Result> call = request.tambahrfid("0");
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
//                loading.dismiss();
                if (value.equals("1")) {
                    //getRFID();
                    Toast.makeText(TambahRfid.this, message, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TambahRfid.this, message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                // progress.dismiss();
                Toast.makeText(TambahRfid.this, "Jaringan Error!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getRFID() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService request1 = retrofit.create(ApiService.class);
        Call<JSONResponse> call1 = request1.getRFID();
        call1.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call1, Response<JSONResponse> response) {
                JSONResponse jsonResponse1 = response.body();
                data1 = new ArrayList<>(Arrays.asList(response.body().getRfid()));
                Log.d("data", data1.get(0).getId_sapi());
                Log.d("jumlah data", String.valueOf(data1.size()));
                if(data1 == null){
                    //avLoadingIndicatorView.smoothToShow();
                    Toast.makeText(TambahRfid.this, "tunggu", Toast.LENGTH_SHORT).show();
                }
                if(jsonResponse1.getRfid().length != 0 && data1.get(0).getId_sapi() != "0") {
                    avLoadingIndicatorView.smoothToHide();
                    final Dialog alert1 = new Dialog(TambahRfid.this, android.R.style.Theme_Black_NoTitleBar);
                    alert1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alert1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#99000000")));
                    alert1.setContentView(R.layout.alert_rfid);
                    alert1.setCancelable(true);

                    TextView tisi = (TextView)alert1.findViewById(R.id.isi);
                    Button bnew = (Button)alert1.findViewById(R.id.btnnew);
                    Button bupd = (Button)alert1.findViewById(R.id.btnupdate);
                    ImageView iclose = (ImageView)alert1.findViewById(R.id.close);

                    tisi.setText(data1.get(0).getId_sapi());
                    iclose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alert1.cancel();
                        }
                    });
                    bnew.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(getApplicationContext(), TambahData.class);
                            i.putExtra("id_sapi", data1.get(0).getId_sapi());
                            i.putExtra("id_user", iduser);
                            startActivity(i);
                        }
                    });

                    bupd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(getApplicationContext(), Manajemen.class);
                            i.putExtra("id_sapi", data1.get(0).getId_sapi());
                            i.putExtra("id_user", iduser);
                            startActivity(i);
                        }
                    });
                    alert1.show();
                }else {
                    Toast.makeText(TambahRfid.this, "gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }


}
