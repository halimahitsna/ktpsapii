package id.sapi.ktp.aplikasiktpsapi.activities;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.orm.SugarApp;
import com.orm.SugarContext;

public class Sugars extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
        //Realm.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
