package id.sapi.ktp.aplikasiktpsapi.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.api.ApiService;
import id.sapi.ktp.aplikasiktpsapi.api.UtilsApi;
import id.sapi.ktp.aplikasiktpsapi.modal.Result;
import id.sapi.ktp.aplikasiktpsapi.util.SharedPrefManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HalamanLogin extends AppCompatActivity {

    Button btnlogin, btnDaftar;
    EditText Euser, Epass;
    ProgressDialog loading;
    AVLoadingIndicatorView circleload;
    Context mContext;
    Toolbar toolbar;
    TextView textToolbar;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    public int log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_login);

        if(getIntent().hasExtra("berhasil")){
            log = getIntent().getIntExtra("berhasil",0);
        }

        koneksi();
        mContext = this;
        mApiService = UtilsApi.getAPIService();
        sharedPrefManager = new SharedPrefManager(this);
        if(sharedPrefManager.getSPSudahLogin() == true){
            startActivity(new Intent(mContext, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }else if(log != 1){
            startActivity(new Intent(mContext, SplashScreen.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
        textToolbar = (TextView)findViewById(R.id.toolbar_title);

        circleload =(AVLoadingIndicatorView)findViewById(R.id.loading);
        circleload.setVisibility(View.INVISIBLE);
        Euser = (EditText) findViewById(R.id.username);
        Epass = (EditText)findViewById(R.id.password);
        btnlogin = (Button)findViewById(R.id.btnMasuk);
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(validate()){
                   circleload.setVisibility(View.VISIBLE);
                   validate();
                   circleload.smoothToShow();
                   login();
                }
            }
        });
        btnDaftar = (Button)findViewById(R.id.linkdaftar);
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, HalamanDaftar.class));
            }
        });

        if(sharedPrefManager.getSPSudahLogin()){
            startActivity(new Intent(HalamanLogin.this, MainActivity.class)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }
    private boolean validate(){
            boolean valid = true;
            String user = Euser.getText().toString();
            String password = Epass.getText().toString();

            if(user.isEmpty()){
                Euser.setError("Username masih kosong!");
                valid = false;
            }else {
                Euser.setError(null);
            }
            if(password.isEmpty()){
                Epass.setError("Password masih kosong!");
                valid = false;
            }
            return valid;
    }

    private void login(){
        koneksi();
        mApiService.loginRequest(Euser.getText().toString(), Epass.getText().toString())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()){
                            circleload.smoothToHide();
                            try{
                                JSONObject jsonRESULT = new JSONObject(response.body().string());
                                if(jsonRESULT.getString("error").equals("false")){
                                   // Toast.makeText(mContext, "Berhasil Login", Toast.LENGTH_SHORT).show();
                                    String user = jsonRESULT.getJSONObject("user").getString("user");
                                    sharedPrefManager.saveSPString(SharedPrefManager.KEY_USER_USER, user);
                                    String id_user= jsonRESULT.getJSONObject("user").getString("id_user");
                                    sharedPrefManager.saveSPString(SharedPrefManager.KEY_USER_ID, id_user);
                                    sharedPrefManager.saveSPBoolean(SharedPrefManager.KEY_LOGIN, true);
                                    addPeternakan(id_user);
                                    new StyleableToast
                                            .Builder(HalamanLogin.this)
                                            .text("Berhasil Masuk")
                                            .iconStart(R.drawable.ic_check_black_24dp)
                                            .textColor(Color.WHITE)
                                            .backgroundColor(Color.BLUE)
                                            .show();
                                    startActivity(new Intent(mContext, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }else {
                                    String error_message= jsonRESULT.getString("error_msg");
                                    new StyleableToast
                                            .Builder(HalamanLogin.this)
                                            .text(error_message)
                                            .iconStart(R.drawable.ic_close_black_24dp)
                                            .textColor(Color.WHITE)
                                            .backgroundColor(Color.RED)
                                            .show();
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else {
                            loading.dismiss();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("debug", "OnFailure: ERROR > " +t.toString());
                        circleload.smoothToHide();
                    }
                });
    }

    private void addPeternakan(String iduser) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UtilsApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService api = retrofit.create(ApiService.class);
        Call<Result> call = api.insertPeternakan(iduser,"Nama Peternakan");
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                String value = response.body().getValue();
                String message = response.body().getMessage();
//                loading.dismiss();
                if (value.equals("1")) {
                    //Toast.makeText(HalamanLogin.this, message, Toast.LENGTH_SHORT).show();
                }else {

                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Toast.makeText(HalamanLogin.this, "Jaringan Error!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private boolean adaInternet(){
        ConnectivityManager koneksi = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
//            Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            new StyleableToast
                    .Builder(HalamanLogin.this)
                    .text("Tidak Ada Koneksi Internet")
                    .iconStart(R.drawable.ic_cloud_off_black_24dp)
                    .textColor(Color.WHITE)
                    .backgroundColor(Color.RED)
                    .show();
        }
    }


}
