package id.sapi.ktp.aplikasiktpsapi.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.DrawableContainer;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.text.Line;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import az.plainpie.PieView;
import az.plainpie.animation.PieAngleAnimation;
import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.activities.DataJenis;
import id.sapi.ktp.aplikasiktpsapi.activities.Peternakan;
import id.sapi.ktp.aplikasiktpsapi.edit.EditProfil;
import id.sapi.ktp.aplikasiktpsapi.util.SharedPrefManager;

public class Pengaturan extends Fragment {
    SharedPrefManager sharedPrefManager;
    LinearLayout ltetang, lver, lpeternakan;
    Switch switchnotif;
    String iduser;
    TextView txstatus;
    SharedPreferences sharedPreferencesNotification;
    public static final String prefnotif = "notif";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.activity_pengaturan, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Pengaturan");
        koneksi();
        sharedPrefManager = new SharedPrefManager(getActivity());
        iduser = sharedPrefManager.getSPId();

        ltetang=(LinearLayout)view.findViewById(R.id.tentang);
        lpeternakan = (LinearLayout)view.findViewById(R.id.laypeternakan);
        lver = (LinearLayout)view.findViewById(R.id.layver);
        switchnotif = (Switch)view.findViewById(R.id.swnotif);
        txstatus = (TextView)view.findViewById(R.id.txtstatus);
        switchnotif.setChecked(true);
        lpeternakan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent j = new Intent(getActivity(), Peternakan.class);
                j.putExtra("id_user", iduser);
                startActivity(j);
            }
        });
        ltetang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent j = new Intent(getActivity(), Tentang.class);
                startActivity(j);
            }
        });

        lver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "No Update available!", Toast.LENGTH_SHORT);
            }
        });

        sharedPreferencesNotification = getActivity().getSharedPreferences(prefnotif,
                Context.MODE_PRIVATE);
        switchnotif.setChecked(sharedPreferencesNotification.getBoolean("NotifStatus", false));
        if(sharedPreferencesNotification.getBoolean("NotifStatus", false) == true ){
            txstatus.setText("ON");
        }else{
            txstatus.setText("OFF");
        }

        switchnotif.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    txstatus.setText("ON");
                    Toast.makeText(getContext(), "Notif ON", Toast.LENGTH_SHORT);
                    SharedPreferences.Editor editor = sharedPreferencesNotification.edit();
                    editor.putBoolean("NotifStatus", true);
                    editor.commit();
                }   else{
                    txstatus.setText("OFF");
                    Toast.makeText(getContext(), "Notif OFF", Toast.LENGTH_SHORT);
                    SharedPreferences.Editor editor = sharedPreferencesNotification.edit();
                    editor.putBoolean("NotifStatus", false);
                    editor.commit();
                }
            }
        });




        /*tPeternakan = (TextView)view.findViewById(R.id.textView);
        relativeLayoutPeternakan = (RelativeLayout) view.findViewById(R.id.layoutPeternakan);
        relativeLayoutPeternakan.setVisibility(View.GONE);

        tPeternakan.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onClick(View view) {
                tPeternakan.setCompoundDrawablesRelative(null, null, getResources().getDrawable(R.drawable.ic_arrow_drop_up_black_24dp), null);
                relativeLayoutPeternakan.setVisibility(relativeLayoutPeternakan.isShown() ? View.GONE : View.VISIBLE);
            }
        });*/

    }
    private boolean adaInternet(){
        ConnectivityManager koneks = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneks.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
//            Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            new StyleableToast
                    .Builder(getActivity())
                    .text("Tidak Ada Koneksi Internet")
                    .iconStart(R.drawable.ic_cloud_off_black_24dp)
                    .textColor(Color.WHITE)
                    .backgroundColor(Color.RED)
                    .show();

        }
    }
}
