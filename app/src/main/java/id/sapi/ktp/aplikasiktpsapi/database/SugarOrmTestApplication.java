package id.sapi.ktp.aplikasiktpsapi.database;

import android.app.Application;
import android.content.res.Configuration;
import com.orm.SugarApp;
import com.orm.SugarContext;

public class SugarOrmTestApplication extends SugarApp {

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(getApplicationContext());
       // LaporanDB.findWithQuery(LaporanDB.class, "SELECT * FROM LaporanDB WHERE id_user=?","3");
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
