package id.sapi.ktp.aplikasiktpsapi.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.bignerdranch.android.multiselector.MultiSelector;
import com.bignerdranch.android.multiselector.SwappingHolder;
import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

import id.sapi.ktp.aplikasiktpsapi.R;
import id.sapi.ktp.aplikasiktpsapi.activities.DetailLaporan;
import id.sapi.ktp.aplikasiktpsapi.api.ApiService;
import id.sapi.ktp.aplikasiktpsapi.api.JSONResponse;
import id.sapi.ktp.aplikasiktpsapi.api.UtilsApi;
import id.sapi.ktp.aplikasiktpsapi.database.LaporanDB;
import id.sapi.ktp.aplikasiktpsapi.edit.EditJadwal;
import id.sapi.ktp.aplikasiktpsapi.edit.EditLaporan;
import id.sapi.ktp.aplikasiktpsapi.modal.Jadwal;
import id.sapi.ktp.aplikasiktpsapi.modal.JadwalAdapter;
import id.sapi.ktp.aplikasiktpsapi.modal.Pakan;
import id.sapi.ktp.aplikasiktpsapi.tambah.TambahJadwal;
import id.sapi.ktp.aplikasiktpsapi.util.AlarmReceiver;
import id.sapi.ktp.aplikasiktpsapi.util.DateTimeSorter;
import id.sapi.ktp.aplikasiktpsapi.util.Reminder;
import id.sapi.ktp.aplikasiktpsapi.util.ReminderDatabase;
import id.sapi.ktp.aplikasiktpsapi.util.SharedPrefManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class JadwalMakan extends Fragment {
    private RecyclerView mList;
    private SimpleAdapter mAdapter;
    private TextView mNoReminderView;
    private FloatingActionButton mAddReminderButton;
    private LinkedHashMap<Integer, Integer> IDmap = new LinkedHashMap<>();
    private ReminderDatabase rb;
    //private MultiSelector mMultiSelector = new MultiSelector();
    private AlarmReceiver mAlarmReceiver;
    SwipeRefreshLayout swipeRefreshLayout;
    SharedPrefManager sharedPrefManager;
    String iduser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_jadwal_makan, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Jadwal Pakan");
        koneksi();

        sharedPrefManager = new SharedPrefManager(getActivity());
        iduser = sharedPrefManager.getSPId().toString();

        rb = new ReminderDatabase(getActivity().getApplicationContext());

        mAddReminderButton = (FloatingActionButton) view.findViewById(R.id.add_reminder);
        mList = (RecyclerView)view.findViewById(R.id.reminder_list);
        mNoReminderView = (TextView) view.findViewById(R.id.no_reminder_text);
        swipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swiperefresh);

        // To check is there are saved reminders
        // If there are no reminders display a message asking the user to create reminders
        //List<Reminder> mTest = rb.getAllReminders();
        List<Reminder> mTest = rb.getReminderByIduser(iduser.trim());
//        Toast.makeText(getContext(), String.valueOf(rb.getRemindersCount()), Toast.LENGTH_SHORT);

        if (mTest.isEmpty()) {
            mNoReminderView.setVisibility(View.VISIBLE);
        }else{
            mNoReminderView.setVisibility(View.GONE);
        }

        // Create recycler view
        loadData();
        // On clicking the floating action button
        mAddReminderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), TambahJadwal.class);
                intent.putExtra("id_user", iduser);
                startActivity(intent);
            }
        });

        // Initialize alarm
        mAlarmReceiver = new AlarmReceiver();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
    }

    public void loadData(){
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setColorSchemeResources(R.color.Card1, R.color.Card2, R.color.Card3, R.color.Card4);
        mList.setLayoutManager(getLayoutManager());
        registerForContextMenu(mList);
        mAdapter = new SimpleAdapter();
        swipeRefreshLayout.setRefreshing(false);
        mAdapter.setItemCount(getDefaultItemCount());
        mList.setAdapter(mAdapter);
    }

    // On clicking a reminder item
    private void selectReminder(int mClickID) {
        String mStringClickID = Integer.toString(mClickID);

        // Create intent to edit the reminder
        // Put reminder id as extra
        Intent i = new Intent(getActivity(), EditJadwal.class);
        i.putExtra(EditJadwal.EXTRA_REMINDER_ID, mStringClickID);
        startActivityForResult(i, 1);
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        mAdapter.setItemCount(getDefaultItemCount());
//    }
//
//    // Recreate recycler view
//    // This is done so that newly created reminders are displayed
//    @Override
//    public void onResume(){
//        super.onResume();
//
//        // To check is there are saved reminders
//        // If there are no reminders display a message asking the user to create reminders
//        List<Reminder> mTest = rb.getAllReminders();
//
//        if (mTest.isEmpty()) {
//            mNoReminderView.setVisibility(View.VISIBLE);
//        } else {
//            mNoReminderView.setVisibility(View.GONE);
//        }
//
//        mAdapter.setItemCount(getDefaultItemCount());
//    }

    // Layout manager for recycler view
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
    }

    protected int getDefaultItemCount() {
        return 100;
    }

    // Create menu

    // Adapter class for recycler view
    public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.VerticalItemHolder> {
        private ArrayList<ReminderItem> mItems;
        public int colorBefore;
        public SimpleAdapter() {
            mItems = new ArrayList<>();
        }

        public void setItemCount(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
            notifyDataSetChanged();
        }

        public void onDeleteItem(int count) {
            mItems.clear();
            mItems.addAll(generateData(count));
        }

        public void removeItemSelected(int selected) {
            if (mItems.isEmpty()) return;
            mItems.remove(selected);
            notifyItemRemoved(selected);
        }

        // View holder for recycler view items
        @Override
        public VerticalItemHolder onCreateViewHolder(ViewGroup container, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(container.getContext());
            View root = inflater.inflate(R.layout.isi_jadwal, container, false);

            return new VerticalItemHolder(root, this);
        }

        @Override
        public void onBindViewHolder(VerticalItemHolder itemHolder, int position) {
            ReminderItem item = mItems.get(position);
            itemHolder.setReminderTitle(item.mTitle);
            itemHolder.setReminderDateTime(item.mDateTime +" WIB");
            itemHolder.mRepeatInfoText.setText(item.mPakan+" "+item.mJumlah+" kg "+item.mKandang);
//            itemHolder.setReminderRepeatInfo(item.mPakan, item.mJumlah, item.mKandang);
            //itemHolder.setActiveImage(item.mNote);
            //itemHolder.setReminderRepeatInfo(item.mRepeat, item.mRepeatNo, item.mRepeatType);
            //itemHolder.setActiveImage(item.mActive);
            Toast.makeText(getContext(),"Repeat "+item.mRepeatType, Toast.LENGTH_SHORT);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        // Class for recycler view items
        public  class ReminderItem {

            public String mTitle;
            public String mDateTime;
            public String mRepeat;
            public String mRepeatNo;
            public String mRepeatType;
            public String mActive;
            public String mNote;
            public String iduser;
            public String mPakan;
            public String mJumlah;
            public String mKandang;

            public ReminderItem(String Title, String DateTime, String Repeat, String RepeatNo, String RepeatType, String Active,String Note,String id_user, String Pakan, String Jumlah, String Kandang) {
                this.mTitle = Title;
                this.mDateTime = DateTime;
                this.mRepeat = Repeat;
                this.mRepeatNo = RepeatNo;
                this.mRepeatType = RepeatType;
                this.mActive = Active;
                this.mNote = Note;
                this.iduser = id_user;
                this.mPakan = Pakan;
                this.mJumlah = Jumlah;
                this.mKandang = Kandang;

            }
        }

        // Class to compare date and time so that items are sorted in ascending order
        public class DateTimeComparator implements Comparator {
            DateFormat f = new SimpleDateFormat("dd/mm/yyyy hh:mm");

            public int compare(Object a, Object b) {
                String o1 = ((DateTimeSorter)a).getDateTime();
                String o2 = ((DateTimeSorter)b).getDateTime();

                try {
                    return f.parse(o1).compareTo(f.parse(o2));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }

        // UI and data class for recycler view items
        public  class VerticalItemHolder extends  RecyclerView.ViewHolder
                implements View.OnClickListener {
            private TextView mTitleText, mDateAndTimeText, mRepeatInfoText, ternaktextinfo;
            private ImageView mActiveImage , mThumbnailImage, iDel, iEdit;
            private ColorGenerator mColorGenerator = ColorGenerator.DEFAULT;
            private TextDrawable mDrawableBuilder;
            private SimpleAdapter mAdapter;

            public VerticalItemHolder(final View itemView, SimpleAdapter adapter) {
                super(itemView);

                // Initialize adapter for the items
                mAdapter = adapter;

                // Initialize views
                ternaktextinfo = (TextView)itemView.findViewById(R.id.PakanJumlah) ;
                mTitleText = (TextView) itemView.findViewById(R.id.recycle_title);
                mDateAndTimeText = (TextView) itemView.findViewById(R.id.recycle_date_time);
                mRepeatInfoText = (TextView) itemView.findViewById(R.id.recycle_repeat_info);
                mActiveImage = (ImageView) itemView.findViewById(R.id.active_image);
                mThumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);
                iDel = (ImageView)itemView.findViewById(R.id.hapus);
                iEdit = (ImageView)itemView.findViewById(R.id.edit);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = getAdapterPosition();
                        // check if item still exists
                        if(pos != RecyclerView.NO_POSITION){
                            final Dialog alert1 = new Dialog(itemView.getRootView().getContext(), android.R.style.Theme_Black_NoTitleBar);
                            alert1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            alert1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#99000000")));
                            alert1.setContentView(R.layout.custom_alert1);
                            alert1.setCancelable(true);

                            TextView tjudul = (TextView)alert1.findViewById(R.id.title);
                            TextView tsub = (TextView)alert1.findViewById(R.id.subjudul);
                            TextView tsub1 = (TextView)alert1.findViewById(R.id.subjudul1);
                            TextView tisi = (TextView)alert1.findViewById(R.id.isi);
                            ImageView foto = (ImageView)alert1.findViewById(R.id.gbr);
                            TextView isi2 = (TextView)alert1.findViewById(R.id.isi2);
                            foto.setVisibility(View.GONE);
                            isi2.setVisibility(View.GONE);
                            tsub1.setVisibility(View.GONE);

                            tjudul.setText("Detail Jadwal Pakan");
                            tsub.setVisibility(View.GONE);
                            tisi.setText(mItems.get(pos).mTitle +"\nTanggal         : "+mItems.get(0).mDateTime+"\nPakan            : "+mItems.get(0).mPakan+"\nJumlah          : "+mItems.get(0).mJumlah+" kg\nKandang         : "+mItems.get(0).mKandang);
                            Button bkembali = (Button)alert1.findViewById(R.id.kmb);
                            bkembali.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    alert1.cancel();
                                }
                            });
                            alert1.show();
                        }
                    }

                });

                iDel.setOnClickListener(this);
                iEdit.setOnClickListener(this);
            }

            // On clicking a reminder item
            @Override
            public void onClick(View v) {
//                mTempPost = mList.getChildAdapterPosition(v);

                final int posisi = getAdapterPosition();
                if (v.getId() == iEdit.getId()) {
                    //mTempPost = mList.getChildAt(v);

                    int mReminderClickID = IDmap.get(mList.getChildAdapterPosition(itemView));
                    selectReminder(mReminderClickID);
                } else if (v.getId() == iDel.getId()) {
                    if (posisi != RecyclerView.NO_POSITION) {
                        AlertDialog.Builder alertbox = new AlertDialog.Builder(v.getRootView().getContext());
                        alertbox.setMessage("Apakah Anda yakin akan menghapus data ini?");
                        alertbox.setTitle("Warning");
                        alertbox.setIcon(R.drawable.ic_warning_black_24dp);

                        alertbox.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //final Reminder reminder = mItems.get(posisi);
//                                if (mItems.isEmpty()) return;
//                                mItems.remove(getAdapterPosition());
//                                notifyItemRemoved(getAdapterPosition());
//                                mItems.clear();
                                int id = IDmap.get(getAdapterPosition());

                                // Get reminder from reminder database using id
                                Reminder temp = rb.getReminder(id);
                                // Delete reminder

                                // Remove reminder from recycler view
//                                mAdapter.removeItemSelected(i);
                                rb.deleteReminder(temp);
                                notifyItemRemoved(id);

                                mAlarmReceiver.cancelAlarm(getContext(), id);
                                mAdapter.onDeleteItem(getDefaultItemCount());

                                // Display toast to confirm delete
                                Toast.makeText(getContext(),
                                        "Deleted",
                                        Toast.LENGTH_SHORT).show();

                                // To check is there are saved reminders
                                // If there are no reminders display a message asking the user to create reminders
                                List<Reminder> mTest = rb.getAllReminders();
                                if (mTest.isEmpty()) {
                                    mNoReminderView.setVisibility(View.VISIBLE);
                                } else {
                                    mNoReminderView.setVisibility(View.GONE);
                                }



                            }
                        });
                        alertbox.setNegativeButton("Batal",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface arg0,
                                                        int arg1) {
                                    }
                                });
                        alertbox.show();
                    }
                }

                //if (!mMultiSelector.tapSelection(this)) {
                //  mTempPost = mList.getChildAdapterPosition(v);

                //int mReminderClickID = IDmap.get(mTempPost);
                //selectReminder(mReminderClickID);

                // } else if(mMultiSelector.getSelectedPositions().isEmpty()){
                //     mAdapter.setItemCount(getDefaultItemCount());
                // }
            }

            // On long press enter action mode with context menu
//            @Override
//            public boolean onLongClick(View v) {
//                Context activity = getActivity().getApplicationContext();
//               // activity.getApplicationContext().startSupportActionMode(mDeleteMode);
//                mMultiSelector.setSelected(this, true);
//                return true;
//            }

            // Set reminder title view
            public void setReminderTitle(String title) {
                mTitleText.setText(title);
                String letter = "A";

                if(title != null && !title.isEmpty()) {
                    letter = title.substring(0, 1);
                }

                int color = mColorGenerator.getRandomColor();

                // Create a circular icon consisting of  a random background colour and first letter of title
                mDrawableBuilder = TextDrawable.builder()
                        .buildRound(letter, color);
                mThumbnailImage.setImageDrawable(mDrawableBuilder);
            }

            // Set date and time views
            public void setReminderDateTime(String datetime) {
                mDateAndTimeText.setText(datetime);
            }

            // Set repeat views
            public void setReminderRepeatInfo(String repeat, String repeatNo, String repeatType) {
                if(repeat.equals("true")){
                    mRepeatInfoText.setText("Every " + repeatNo + " " + repeatType + "(s)");
                }else if (repeat.equals("false")) {
                    mRepeatInfoText.setText("Repeat Off");
                }
            }

            public void setPakan(String pakan) {
                mRepeatInfoText.setText(pakan);
            }

            // Set active image as on or off
            public void setActiveImage(String active){
                if(active.equals("true")){
                    mActiveImage.setImageResource(R.drawable.ic_notifications_active_black_24dp);
                }else if (active.equals("false")) {
                    mActiveImage.setImageResource(R.drawable.ic_notifications_off_black_24dp);
                }
            }
        }

        // Generate random test data
        public  ReminderItem generateDummyData() {
            return new ReminderItem("1","1", "2", "3", "4", "5", "6","7","8","9","10");
        }

        // Generate real data for each item
        public List<ReminderItem> generateData(int count) {
            ArrayList<SimpleAdapter.ReminderItem> items = new ArrayList<>();

            // Get all reminders from the database
            List<Reminder> reminders = rb.getReminderByIduser(iduser.trim());

            // Initialize lists
            List<String> Titles = new ArrayList<>();
            List<String> Repeats = new ArrayList<>();
            List<String> RepeatNos = new ArrayList<>();
            List<String> RepeatTypes = new ArrayList<>();
            List<String> Actives = new ArrayList<>();
            List<String> Note = new ArrayList<>();
            List<String> DateAndTime = new ArrayList<>();
            List<Integer> IDList= new ArrayList<>();
            List<DateTimeSorter> DateTimeSortList = new ArrayList<>();
            List<String> IDUser = new ArrayList<>();
            List<String> Pakan = new ArrayList<>();
            List<String> Jumlah = new ArrayList<>();
            List<String> Kandang = new ArrayList<>();

            // Add details of all reminders in their respective lists
            for (Reminder r : reminders) {
                Titles.add(r.getTitle());
                DateAndTime.add(r.getDate() + " " + r.getTime());
                Repeats.add(r.getRepeat());
                RepeatNos.add(r.getRepeatNo());
                RepeatTypes.add(r.getRepeatType());
                Actives.add(r.getActive());
                Note.add(r.getmNote());
                IDUser.add(r.getIduser());
                Pakan.add(r.getmPakan());
                Jumlah.add(r.getmJumlah());
                Kandang.add(r.getmKandang());
                IDList.add(r.getID());
            }

            int key = 0;

            // Add date and time as DateTimeSorter objects
            for(int k = 0; k<Titles.size(); k++){
                DateTimeSortList.add(new DateTimeSorter(key, DateAndTime.get(k)));
                key++;
            }

            // Sort items according to date and time in ascending order
            Collections.sort(DateTimeSortList, new DateTimeComparator());

            int k = 0;

            // Add data to each recycler view item
            for (DateTimeSorter item:DateTimeSortList) {
                int i = item.getIndex();

                items.add(new SimpleAdapter.ReminderItem(Titles.get(i), DateAndTime.get(i), Repeats.get(i),
                        RepeatNos.get(i), RepeatTypes.get(i), Actives.get(i),Note.get(i),IDUser.get(i), Pakan.get(i), Jumlah.get(i),Kandang.get(i)));
                IDmap.put(k, IDList.get(i));
                k++;
            }
            return items;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        loadData();
    }

    @Override
    public void onResume(){
        super.onResume();
        mNoReminderView.setVisibility(View.GONE);
        loadData();
    }

    private boolean adaInternet(){
        ConnectivityManager koneksi = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return koneksi.getActiveNetworkInfo() != null;
    }
    private void koneksi(){
        if(adaInternet()){
//            Toast.makeText(HalamanUtama.this, "Terhubung ke internet", Toast.LENGTH_LONG).show();
        }else{
            new StyleableToast
                    .Builder(getActivity())
                    .text("Tidak Ada Koneksi Internet")
                    .iconStart(R.drawable.ic_cloud_off_black_24dp)
                    .textColor(Color.WHITE)
                    .backgroundColor(Color.RED)
                    .show();
            mNoReminderView.setVisibility(View.VISIBLE);
            mNoReminderView.setText("Tidak ada koneksi internet!");
        }
    }
}
